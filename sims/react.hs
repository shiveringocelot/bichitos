
data Atom = AtomUp | AtomDown | AtomLeft | AtomRight deriving Eq
data Axis = Vertical | Horizontal deriving Eq
data Alignment = Aligned | Unaligned | Disaligned deriving Eq

axis AtomUp = Vertical
axis AtomDown = Vertical
axis AtomLeft = Horizontal
axis AtomRight = Horizontal


alignment a b | axis a == axis b =
    if a == b then Aligned else Disaligned
alignment _ _ = Unaligned


data Fiber = Fiber Int Atom

add (Fiber 0 a) b = Fiber 0 a


main = do
  putStrLn "Hello World!"

