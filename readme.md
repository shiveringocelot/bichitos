
# Chemistry

Quarks are the most elementary unit, they can be up or down. Atoms are made of a magnitude and a set of 8 quarks.

Combining two atoms `a` and `b` results in an atom `c` such that:
- `c.mag` is `lerp(a.mag + b.mag, abs(a.mag - b.mag), affinity(a, b))`
- for each quark `c_i` in `c`, and the correspinding quarks `a_i` and `b_i`:
    - `c_i` is `a_i` with a probability of `a.mag / (a.mag + b.mag)`
    - `c_i` is `b_i` with a probability of `b.mag / (a.mag + b.mag)`

The affinity of two atoms is their proportion of quarks matching place and direction.

# Bichos

## Energy

Bichos store energy in their bellies, which can be seen at the back of their center. Energy has a mass and makes the creatures need more force to move at the same speed.

## Metabolism

The bichos have a rotating inner clock whose frequency they control with their metabolism. Controlling the metabolism has no energy cost. but it's central frequency matches the generation frequency.

The clock appears as a small oscillating body part, going back and front, at the front of the center of the Bicho.

## Age

A bichos condition, which is reflected by their capacity to regenerate, gets depleted over time. That's their age.

Bichos can convert their stored energy into health (I don't know how health reflects yet in their survival odds). The environment hurts the creature every generation, the more age they have the less they can restore health. Every time they restore health, they increase a proportional amount of age.

One way health could affect is by inducing pain, and giving them negative reward function for their Reinforcement Learning.

# Genetics

The genes for the bichos have parameters for their neuron weights and biases, their mutation rate (and each kind of mutation rate).

When a bicho divides, it splits its energy with another bicho and the new one has a small chance to mutate. If there are memory cells, their values also copy.

# Deadpool

The deadpool is a competition for dead bichos, where "divine selection" happens, because in the beginning natural evolution is too slow.

Everytime a bicho dies, it tries to enter the deadpool. The creature will be matched against a random sample from the pool and made to compete. If the dead creature has better fitness than the randomly selected sample, it will take that place in the pool. The new bicho also takes the place regardless of competition 1% of the time, to allow diversity. This indirectly benefits more fertile bichos as well.

Naturally spawned bichos have a 5% chance of being sampled from the deadpool, and 95% of having novel genes.

The deadpool fitness is the addition of:
- The age at which the bicho died, scaled to 0..2
- The number of offspring, scaled to 0..1

# Brain

Bichos have as a brain an evolving neural network ([NEAT](https://nn.cs.utexas.edu/downloads/papers/stanley.cec02.pdf)) that has these inputs:

- Health (0 to 1)
- Logarithmic energy level
- Logarithmic age
- Forward momentum
- Side momentum
- Sine of the inner clock's phase
- Cosine of the inner clock's phase
- Antenna sensor read
- An input for each memory

For the outputs:

- Forward force
- Rotation (how much to rotate, not a force)
- How much material to eat
- How much to heal
- Whether to split
- How much material to deposit
- Metabolism, how many ticks to sleep
- Inner clock rate
- Antenna alignment with the skin
- Antenna alignment with each gland
- An output for each memory

# Roadmap

- Shader for cell map (faster update)

- Moving cell sources
- Add chemistry system

- NEAT Speciation
- Show synapse width in brain graph
- Add symbols for brain inputs and outputs
- World parameter controls

# Resources

- https://caballerocoll.com/blog/bevy-rhythm-game/
- https://www.youtube.com/watch?v=neyIpnII-WQ

# Extensions

## Sound

Bichos can shout at a chosen frequency, all bichos near him can hear it depending on their distance and tuned frequency.

Frecuency space should be a circular space. A good frecuency sensitivity formula can be:
`2/(abs((f+1)x/f) + 1) - 1`

Where `x` is the signed normalized frequency difference (range -1 to 1, if absolute, abs can be removed), and f is the force applied. Negative results are replaced with 0.
With `f=1`, the sensitivity ranges half of the frequency space, and limits to all the frequency at infinity with diminishing returns.

## Size

Size is a controllable parameter. It adds to the mass, but it raises the threshold of energy they can eat efficiently.

## Skin/Cell reactions

Every tick, take an *ABSORTION_RATE* drop from the map cell, and a *SHED_RATE* drop from the bicho skin (which multiplies with their age), and make the map cell and the skin react with each other drop, the cell normally but the skin aligns its reaction with the skin.

Alignment is when an atom is forced to take another's alignment. It removes from the magnitude the proportion of bits that needed to be flipped. I.e. multiplies the magnitude by the affinity.

## Age and Degeneration

A bichos degeneration rate is given directly by their age, but how it affects is given by genetic parameters, that have a weight for each kind of degeneration: antenna size, skin shedding.

## Map Difussion

Every generation, every cell in the map loses a bit of its mass to the elements, and another bit that mixes with neighbor cells (up and right)

## Additional Neurons

In theory simple neural networks are universal function aproximators, however evolving complex functions one neuron at a time is prone to fail in an evolutionary process with constant environment pressure. Special neuron types might help the bichos develop more complex behaviors more robustly.
- Delta: outputs the difference of the input sum between ticks
- Resistor/Attack: integrates a portion of the circuit per tick, taking sustained input signals to fully activate.
- Capacitor/Release: takes the input sum value as charge when it's higher, and disspates charge otherwise, taking any high input to activate and sustained low inputs to rest.
- Boolean: Outputs 1 if the input signal sum reaches a threshold and 0 otherwise.