
use rand::{thread_rng, Rng};
use crate::neat;
use crate::constants::{
	MEMORY_DECAY, INITIAL_BRAIN_CONNECTIVITY
};

const TAU: f32 = 3.141592 * 2.0;

fn sigmoid (x: f32) -> f32 { 1.0 / ((-x).exp() + 1.0) }
fn halfmoid (x: f32) -> f32 { 2.0 / ((-x).exp() + 1.0) - 1.0 }

// Scales down values that grow to infinity
fn scalelong (x: f32) -> f32 { x / (x.abs() + 1.0) }

#[derive(Clone)]
pub struct Brain {
	pub genome: neat::Genome,
	pub network: neat::Network,
	pub memory_cell_count: usize,
	pub values: Vec<f32>,
}

#[derive(Default)]
pub struct BrainInput {
	pub age: f32,
	pub energy: f32,
	pub thickness: f32,
	pub antenna_strength: f32,
	pub antenna_affinity: f32,
	pub clock_sin: f32,
	pub clock_cos: f32,
	pub speed: f32,
}

impl BrainInput {
	const COUNT: usize = 8;
	fn to_values (&self) -> [f32; Self::COUNT] {[
		scalelong(self.age / 100.0),
		scalelong(self.energy / 1.0),
		scalelong(self.thickness / 1.0),
		scalelong(self.antenna_strength),
		self.antenna_affinity,
		self.clock_sin,
		self.clock_cos,
		scalelong(self.speed),
	]}

	/// Checks that all fields have valid values and panics if any doesn't
	fn check (&self) {
		let mut bad_fields = vec![];

		macro_rules! check_fields {
			($($field_name:ident),+) => {
				$(
					if !self.$field_name.is_finite() {
						bad_fields.push(stringify!($field_name));
					}
				)+
			};
		}

		check_fields!(
			age, energy, thickness,
			antenna_strength, antenna_affinity,
			clock_sin, clock_cos, speed
		);

		if !bad_fields.is_empty() {
			panic!("bad brain inputs: {}", bad_fields.join(", "));
		}
	}
}

pub struct BrainOutput {
	pub angle: f32,
	pub force: f32,
	pub heal: f32,
	pub antenna_angle: f32,
	pub antenna_length: f32,
	pub antenna_size: f32,
	pub eat: bool,
	pub deposit: bool,
	pub split: bool,
	pub clock_rate: f32,
}

impl BrainOutput {
	const COUNT: usize = 10;
	fn from_values (values: [f32; Self::COUNT]) -> Self {
		let mut iter = values.into_iter();
		let mut next = || iter.next().unwrap();
		BrainOutput {
			angle: next() * TAU * 0.5,
			force: next(),
			heal: next(),
			antenna_angle: halfmoid(next()),
			antenna_length: next(),
			antenna_size: next().max(0.0),
			// They eat by default
			eat: next() < 0.5,
			deposit: next() > 0.5,
			split: next() > 0.5,
			clock_rate: next().exp(),
		}
	}
}

#[derive(Debug, Clone)]
pub struct NeuronDraw {
	pub value: f32,
	pub bias: f32,
	pub pos: (u32, u32),
}

#[derive(Debug, Clone)]
pub struct SynapseDraw {
	pub value: f32,
	pub weight: f32,
	pub start: (u32, u32),
	pub end: (u32, u32),
}

#[derive(Debug, Clone)]
pub struct BrainDraw {
	pub size: (u32, u32),
	pub neurons: Vec<NeuronDraw>,
	pub synapses: Vec<SynapseDraw>,
}

impl Brain {
	pub fn new (memory_cell_count: usize) -> Brain {
		let mut genome = neat::Genome::create_initial(
			// Constant signal
			BrainInput::COUNT + memory_cell_count + 1,
			BrainOutput::COUNT + memory_cell_count
		);
		let network = genome.to_network();
		let values = vec![0.0; network.nodes.len()];
		Brain {genome, network, values, memory_cell_count}
	}

	pub fn update_network (&mut self) {
		self.network = self.genome.to_network();
		self.values = vec![0.0; self.network.nodes.len()];
	}

	pub fn randomize_weights (&mut self) {
    	let mut rng = thread_rng();

		for connection in &mut self.genome.connections {
			if rng.gen_bool(INITIAL_BRAIN_CONNECTIVITY as f64) {
				connection.weight = rng.gen_range(-1.0..1f32);
			} else {
				connection.enabled = false;
			}
		}

		self.update_network();
	}

	pub fn set_input (&mut self, index: u32, value: f32) {
		if let Some(index) = self.network.inputs[index as usize] {
			self.values[index] = value;
		}
	}

	pub fn get_output (&self, index: u32) -> f32 {
		if let Some(index) = self.network.outputs[index as usize] {
			self.values[index]
		} else { 0.0 }
	}

	pub fn process (&mut self, input: BrainInput) -> BrainOutput {
		input.check();

		// Constant signal, for the bias
		if let Some(bias_index) = self.network.inputs[0] {
			self.values[bias_index] = 1.0;
		}

		let in_values = input.to_values();
		for i in 0 .. BrainInput::COUNT {
			if let Some(index) = self.network.inputs[i + 1] {
				self.values[index] = in_values[i];
			}
		}

		self.network.forward(&mut self.values);

		let mut out_values = [0.0; BrainOutput::COUNT];
		for i in 0 .. BrainOutput::COUNT {
			if let Some(index) = self.network.outputs[i] {
				out_values[i] = self.values[index];
			}
		}

		// Copy memory outputs to input
		for i in 0 .. self.memory_cell_count {
			if let Some(in_index) = self.network.inputs[BrainInput::COUNT + 1 + i] {
				if let Some(out_index) = self.network.outputs[BrainOutput::COUNT + i] {
					let out_value = self.values[out_index];
					let in_value = &mut self.values[in_index];

					if out_value < 0.0 {
						// If out value is negative, decay memory
						*in_value *= (1.0 - MEMORY_DECAY);
					} else if out_value > *in_value {
						// If out value is greater than current value, interpolate
						*in_value *= (1.0 - MEMORY_DECAY);
						*in_value += out_value * MEMORY_DECAY;
					}
				}
			}
		}

		BrainOutput::from_values(out_values)
	}

	pub fn param_count (&self) -> usize { self.genome.connections.len() }

	pub fn neuron_count (&self) -> usize { self.genome.nodes.len() }

	pub fn compute_energy_use (&self, neuron_energy_use: f32) -> f32 {
		(self.neuron_count() as f32 + self.param_count() as f32 / 2.0) * neuron_energy_use
	}

	pub fn compute_draw (&self) -> BrainDraw {
		let layer_count = self.genome.layers.len();
		let mut draw = BrainDraw {
			size: (layer_count as u32+1, 0),
			neurons: vec![],
			synapses: vec![],
		};

		let mut positions = vec![None; self.network.nodes.len()];

		// Create positions for inputs and outputs
		for (i, index) in self.network.inputs.iter().enumerate() {
			if let Some(index) = *index {
				positions[index] = Some((0, i));
			}
		}

		for (i, index) in self.network.outputs.iter().enumerate() {
			if let Some(index) = *index {
				positions[index] = Some((layer_count, i));
			}
		}

		let mut level_counts = vec![0; layer_count];

		for i in 0..positions.len() {
			if positions[i].is_none() {
				let level = self.network.nodes[i].level;

				positions[i] = Some((level, level_counts[level]));
				level_counts[level] += 1;
			}

			let pos = positions[i].unwrap();

			draw.neurons.push(NeuronDraw {
				value: self.values[i],
				bias: 0.0,
				pos: (pos.0 as u32, pos.1 as u32),
			});
		}

		// Draw synapses
		for (i, node) in self.network.nodes.iter().enumerate() {
			if let Some((x_out, y_out)) = positions[i] {
				for synapse in &node.inputs {
					if let Some((x_in, y_in)) = positions[synapse.input] {
						draw.synapses.push(SynapseDraw {
							value: self.values[synapse.input] * synapse.weight,
							weight: synapse.weight,
							start: (x_in as u32, y_in as u32),
							end: (x_out as u32, y_out as u32),
						});
					}
				}
			}
		}

		draw.size.1 = positions
			.iter()
			.filter_map(|x| *x)
			.map(|(x, y)| y as u32)
			.max()
			.unwrap_or(0) + 1;

		draw
	}

	pub fn debug (&self) {
		for (i, perceptron) in self.network.nodes.iter().enumerate() {
			for synapse in &perceptron.inputs {
				println!(
					"  {:.2} * {:.2}",
					self.values[synapse.input],
					synapse.weight,
				);
			}
			println!("=> {:.2}", self.values[i]);
		}
	}
}

/*
mod tests {
	use super::*;

	#[test]
	fn brain_1_1 () {
		let mut brain = Brain::with_layer_sizes(&[1, 1]);
		let mut neurons = vec![0.0; brain.neuron_count()];

		brain.process(&mut neurons);
		assert_eq!(neurons[1], 0.0);

		neurons[0] = 1.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[1], 0.0);

		brain.weights[0] = 1.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[1], 1.0);

		// Activation
		neurons[0] = -1.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[1], -0.25);

		brain.biases[1] = 2.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[1], 1.0);
	}

	#[test]
	fn brain_2_1 () {
		let mut brain = Brain::with_layer_sizes(&[2, 1]);
		let mut neurons = vec![0.0; brain.neuron_count()];

		brain.process(&mut neurons);
		assert_eq!(neurons[2], 0.0);

		neurons[0] = 1.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[2], 0.0);

		brain.weights[0] = 1.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[2], 1.0);

		neurons[1] = 1.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[2], 1.0);

		brain.weights[1] = 1.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[2], 2.0);

		neurons[0] = -1.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[2], 0.0);
	}

	#[test]
	fn xor () {
		let mut brain = Brain::with_layer_sizes(&[2, 2, 1]);
		let mut neurons = vec![0.0; brain.neuron_count()];

		// Neuron 1,0 #2 Or gate
		brain.weights[0] = 1.0;
		brain.weights[1] = 1.0;
		// Values: 0, 1, 1, 2

		// Neuron 1,1 #3 And gate
		brain.biases[3] = -1.0;
		brain.weights[2] = 1.0;
		brain.weights[3] = 1.0;
		// Values: -0.25, 0, 0, 1

		// Neuron 2,0 #4 Or - And
		brain.biases[4] = -0.5;
		brain.weights[4] = 1.0;
		brain.weights[5] = -2.0;

		neurons[0] = 0.0;
		neurons[1] = 0.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[4], 0.0);

		neurons[0] = 1.0;
		neurons[1] = 0.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[4], 0.5);

		neurons[0] = 0.0;
		neurons[1] = 1.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[4], 0.5);

		neurons[0] = 1.0;
		neurons[1] = 1.0;
		brain.process(&mut neurons);
		assert_eq!(neurons[4], -0.125);
	}

	#[test]
	fn brain_4_2 () {
		let mut brain = Brain::with_layer_sizes(&[4, 2]);
		brain.randomize_weights();

		let mut neurons = vec![0.0; brain.neuron_count()];
		brain.process(&mut neurons);
	}
}
*/