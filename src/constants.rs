
pub const MAP_SIZE: usize = 384;

/// How many ticks a generation lasts
pub const TICKS_PER_GENERATION: u32 = 25;

/// How much mass capacity each cell adds to the world
pub const CELL_CAPACITY: f32 = 0.25;

// NOW IN CONFIG
/// How many cells grow in an empty map
pub const CELL_GROW_RATE: f32 = 500.0;

/// How many generations a bicho takes to become mature
pub const BICHO_MATURITY: f32 = 20.0;

/// How much energy bichos need just to exist at maturity
pub const LIVING_COST: f32 = 1e-5;

/// How much of the momentum the creatures keep per tick
pub const DAMPING: f32 = 0.1;

/// How much matter in the map interacts with the skin.
pub const SKIN_PERMEABILITY: f32 = 0.02;

/// How much matter in the skin gets lost to the map at maturity.
pub const SKIN_SHED: f32 = 1e-4;

// NOW IN CONFIG
/// How many cells to try to spawn a bicho on every tick
pub const SPAWN_COUNT: u32 = 200;

/// How much energy does it cost to split a bicho
pub const BIRTH_COST: f32 = 0.5;

/// How much of the current memory output to mix in the bicho's memory
pub const MEMORY_TRANSFER: f32 = 0.06;

/// How many generation samples a history frame covers
pub const HISTORY_FRAME_SIZE: usize = 64;



// BRAIN CONSTANTS
/// How much the value of a memory cell is lost per tick
pub const MEMORY_DECAY: f32 = 2.0 / TICKS_PER_GENERATION as f32;

/// Likelihood in a fresh brain of an input neuron being connected to an output neuron
pub const INITIAL_BRAIN_CONNECTIVITY: f32 = 0.333;

/// How many mutations occur on average every cell split
pub const INITIAL_MUTATION_RATE: f32 = 5.0;



/// How much mass a cell loses per generation
pub const MAP_LOSS: f32 = 0.05;

/// How much mass a cell loses to neighbor cells per generation
pub const MAP_DIFUSSION: f32 = 0.33;





/// How much percentage of new bichos should sample from the deadpool
pub const DEADPOOL_SAMPLE_RATE: f32 = 0.5;

/// How much percentage of new bicho samples compete in the deadpool,
/// compared to being included just by chance.
pub const DEADPOOL_LUCKY_RATE: f32 = 0.05;

/// How many samples the deadpool has space for
pub const DEADPOOL_CAPACITY: usize = 1000;
