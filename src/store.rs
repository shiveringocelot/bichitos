// This plugin is in charge of saving and loading the state of the game in disk

use std::collections::VecDeque;
use std::fs::File;
use std::io::Write;

use serde::{Deserialize, Serialize};
use bevy::{
    prelude::*,
};

use crate::{
    config::Config,
    world::{Map, Time},
    bicho::{self},
    chemistry::Atom,
    constants, units, spawner,
};

#[derive(Event)]
pub struct SaveEvent;

#[derive(Event)]
pub struct LoadEvent;

#[derive(serde::Serialize, serde::Deserialize)]
pub struct SaveData {
    pub config: Config,
    pub map_size: u32,
    pub map_data: String,
    pub spawners: Vec<SpawnerData>,
}

#[derive(serde::Serialize, serde::Deserialize)]
pub struct SpawnerData {
    pub speed: f32,
    pub jitter: f32,
    pub atom: String,
    pub range: f32,
    pub shape: u8,
    pub position: Vec2,
    pub heading: f32,
    /// Each entry in the pool is `(fitness, gene_data)`
    pub pool: Vec<(f32, crate::gene::GeneData)>,
}

fn encode_seq(atoms: impl IntoIterator<Item = impl std::borrow::Borrow<Atom>>) -> String {
    atoms.into_iter()
        .map(|atom| atom.borrow().encode_string())
        .collect::<Vec<String>>()
        .join(";")
}

fn decode_seq(encoded: &str) -> Vec<Atom> {
    encoded.split(';')
        .filter_map(|s| Atom::decode_string(s))
        .collect()
}

fn serialize_all (
    config: &Config, map: &Map, spawners: Vec<SpawnerData>, bichos: ()
) -> String {
    let data = SaveData {
        config: config.clone(),
        map_size: map.size as u32,
        map_data: encode_seq(map.get_cells_ref()),
        spawners,
    };

    return serde_json::to_string(&data).unwrap();
}

fn save_event_listener(
    //mut commands: Commands,
    config: Res<Config>,
    map: Res<Map>,
    spawners: Query<(
        &spawner::component::Spawner,
        &crate::deadpool::Pool,
        &spawner::component::Position,
        &spawner::component::Heading
    )>,
    mut events: EventReader<SaveEvent>
) {
    let mut iter = events.read();
    if let Some(_) = iter.next() {
        /*let save_file: &str = match &config.save_file {
            Some(s) => s,
            None => {
                println!("No save file in Config: 'save_event_listener'");
                return;
            }
        };*/

        println!("TODO: Configurable save file");
        let save_file = "./bichitos-save.json";

        let spawner_data = spawners.iter().map(|(spawner, pool, pos, heading)| SpawnerData {
            speed: spawner.speed,
            jitter: spawner.jitter,
            atom: spawner.atom.encode_string(),
            range: spawner.range,
            shape: 0,
            position: pos.0,
            heading: heading.0,
            pool: pool.get_store_data(),
        }).collect();
        let data = serialize_all(&config, &map, spawner_data, ());

        // Save data to file
        let mut file = File::create(save_file).expect("Failed to create save file");
        file.write_all(data.as_bytes()).expect("Failed to write to save file");
        println!("Game data saved to: {}", save_file);
    }

    // Consume all remaining events, without saving again
    for _ in iter {}
}

fn load_event_listener(
    mut commands: Commands,
    mut config: ResMut<Config>,
    mut map: ResMut<Map>,
    mut events: EventReader<LoadEvent>,
    spawners: Query<(Entity, &spawner::component::Spawner, &crate::deadpool::Pool)>
) {
    let mut iter = events.read();
    if let Some(_) = iter.next() {
        /*let save_file: &str = match &config.save_file {
            Some(s) => s,
            None => {
                println!("No save file in Config: 'save_event_listener'");
                return;
            }
        };*/

        println!("TODO: Configurable save file");
        let save_file = "./bichitos-save.json";

        let file = File::open(save_file).expect("Failed to open save file");
        let data: SaveData = serde_json::from_reader(file).expect("Failed to read save file");

        if map.size as u32 == data.map_size {
            map.set_cells(decode_seq(&data.map_data));
        } else {
            println!("ERROR: Incompatible map size");
        }

        // Replace Spawners
        for (entity, _, _) in &spawners {
            commands.entity(entity).despawn_recursive();
        }
        for spawner in data.spawners {
            commands.spawn((
                spawner::component::Spawner {
                    speed: spawner.speed,
                    jitter: spawner.jitter,
                    atom: Atom::decode_string(&spawner.atom).unwrap(),
                    range: spawner.range,
                    shape: crate::spawner::component::Shape::Center,
                },
                crate::deadpool::Pool::from_store_data(spawner.pool),
                spawner::component::Heading(spawner.heading),
                spawner::component::Position(spawner.position),
            ));
        }
    }

    // Consume all remaining events, without saving again
    for _ in iter {}
}

pub struct Plugin;
impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut App) {
        app//.add_systems(Startup, setup)
            .add_event::<SaveEvent>()
            .add_event::<LoadEvent>()
            .add_systems(Update, (load_event_listener, save_event_listener));
    }
}