
use bevy::{
    prelude::*,
    math::Vec3Swizzles,
    sprite::MaterialMesh2dBundle,
};

use rand::{thread_rng, Rng};

use crate::{
	constants, units,
	gene::Gene,
	world::{Map, Time},
	brain::{Brain, BrainInput},
	chemistry::{Atom, react},
};

pub struct BichoPlugin;
impl Plugin for BichoPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup)
        	.add_event::<SpawnEvent>()

        	//.add_system(spawn_generations)
        	.add_systems(Update, spawn_event_listener)

        	.add_systems(Update, run_inner_clock.after(spawn_event_listener))
        	.add_systems(Update, wake.after(run_inner_clock))

        	.add_systems(Update, update_mass.after(run_inner_clock))
        	.add_systems(Update, react_skin.after(run_inner_clock))
        	.add_systems(Update, read_sensors.after(run_inner_clock))
        	.add_systems(Update, read_speed_sensor.after(run_inner_clock))


        	.add_systems(Update, think
        		.after(run_inner_clock)
        		.after(wake)
        		.after(update_mass)
        		.after(read_sensors)
        		.after(read_speed_sensor)
        	)
        	.add_systems(Update, spend_energy.after(think))
        	.add_systems(Update, react_antenna.after(think))

        	.add_systems(Update, heal.after(spend_energy))
        	.add_systems(Update, self_regulate.after(spend_energy))

        	.add_systems(Update, split.after(spend_energy))
        	.add_systems(Update, kill
        		.after(react_skin)
        		.after(heal)
        	)

        	.add_systems(Update, update_sprites.after(spend_energy))
        	.add_systems(Update, apply_movement.after(spend_energy));
    }
}

const TAU: f32 = 3.141592 * 2.0;

const DELTA: f32 = 1.0 / constants::TICKS_PER_GENERATION as f32;
const DELTA_CLOCK: f32 = TAU * DELTA;

pub mod components {
	use super::*;

	#[derive(Component)]
	pub struct GeneComponent(pub Gene);

	/// Energy stored by the bicho. It dies if this reaches 0
	#[derive(Component, Default)]
	pub struct Energy {
		pub amount: units::Energy,
		pub spent: units::Energy,
	}

	impl Energy {
		pub fn spend (&mut self, amount: f32) -> f32 {
			let spent = amount.min(self.amount.0).max(0.0);
			self.amount.0 -= spent;
			self.spent.0 += spent;
			spent
		}

		pub fn compute_mass (&self) -> f32 {
			self.amount.0 / units::EnergyDensity::BICHO_FUEL.0
		}
	}

	/// How thick is the bicho's skin. This affects how much it reacts with
	/// the environment.
	#[derive(Component, Default)]
	pub struct Thickness(pub f32);

	#[derive(Component, Default)]
	pub struct Mass(pub units::Mass);

	#[derive(Component, Default)]
	pub struct Space(pub units::Energy);


	/// Tells the age and the phase of the internal clock
	#[derive(Component, Default)]
	pub struct InnerClock {
		/// Goes from 0 to infinity, it affects the healing factor inverse-logistical
		pub age: f32,

		/// Goes from 0 to pi. The sine and cosine are inputs for the brain.
		pub phase: f32,

		/// Frequency of the clock in generation length.
		pub rate: f32,
	}

	#[derive(Component, Default)]
	pub struct Awake;

	/// Global momentum
	#[derive(Component, Default)]
	pub struct Momentum(pub Vec2);

	/// The angle of the bicho's global looking direction
	#[derive(Component, Default)]
	pub struct Heading(pub f32);

	/// How much matter present where the antenna is (and its similarity¿ to the antenna's material)
	#[derive(Component, Default)]
	pub struct Sensor {
		pub magnitude: f32,
		pub affinity: f32
	}

	#[derive(Component, Default)]
	pub struct SpeedSensor {
		pub magnitude: f32,
	}

	/// Controls the movement and sensing of the bicho
	#[derive(Component, Default)]
	pub struct MoveControl {
		pub angle: f32,
		pub force: units::Force,
	}

	/// Controls actions
	#[derive(Component, Default)]
	pub struct ActionControl {
		/// Consumes one Mole from the cell where the antenna is
		pub eat: bool,
		/// Deposits one Mole on the cell where the antenna is
		pub deposit: bool,
		/// Whether to self replicate this generation
		pub birth: bool,

		/// How much energy convert to skin mass
		pub heal: f32,
		/// How fast to run the clock. Creature will skip an inverse number of ticks
		pub metabolism: f32,
	}

	#[derive(Component, Default, Debug)]
	pub struct AntennaControl {
		/// The angle of the antenna relative to the looking angle
		pub angle: f32,

		/// How far away is the antenna
		pub length: f32,

		/// How much energy will flow in/out of the antenna
		pub size: f32,
	}

	#[derive(Component)]
	pub struct BrainComponent(pub Brain);

	#[derive(Component)]
	pub enum BodyPart { Belly, Clock, Antenna }

	#[derive(Component, Default)]
	pub struct ChildCount(pub u32);

	#[derive(Component, Default)]
	pub struct MemoryComponent(pub f32);
}

use components::*;

#[derive(Event)]
pub struct SpawnEvent {
	pub position: Vec2,
	pub energy: units::Energy,
	pub gene: Gene,
}


fn get_antenna_position (antenna: &AntennaControl, transform: &Transform) -> Vec2 {
	let body_pos = transform.translation;
	let antenna_rot = transform.rotation * Quat::from_rotation_z(antenna.angle);
	let length = antenna.length.min(constants::MAP_SIZE as f32);
	let local_pos = Vec3::Y * length;
	let antenna_pos = body_pos + antenna_rot * local_pos;
	antenna_pos.truncate()
}

#[derive(Bundle)]
struct BodyBundle {
    sprite: SpriteBundle,

    gene: GeneComponent,
    brain: BrainComponent,

    mass: Mass,
    momentum: Momentum,
    heading: Heading,
    energy: Energy,
    thickness: Thickness,
    space: Space,

    sensor: Sensor,
    speed_sensor: SpeedSensor,
    clock: InnerClock,
    memory: MemoryComponent,

    antenna: AntennaControl,
    move_control: MoveControl,
    action_control: ActionControl,

    child_count: ChildCount,
}

#[derive(Bundle)]
struct BodyPartBundle {
    sprite: SpriteBundle,
    part: BodyPart,
}

const SPRITE_SCALE: f32 = 1.0 / 32.0;

fn setup (
    mut commands: Commands,
    asset_server: Res<AssetServer>,
) {
	// EMPTY
}

fn spawn_event_listener(
    mut commands: Commands,
    asset_server: Res<AssetServer>,
	mut spawn_events: EventReader<SpawnEvent>
) {
    let mut rng = thread_rng();

    for event in spawn_events.read() {
	    let skin_texture = asset_server.load("skin.png");
	    let belly_texture = asset_server.load("belly.png");
	    let clock_texture = asset_server.load("clock.png");
	    let antenna_texture = asset_server.load("limb.png");

	    let gene = event.gene.clone();
        let brain = gene.brain.clone();

	    let color_values = gene.skin.get_chroma_color();
	    let color = Color::rgb(
	    	color_values[0],
	    	color_values[1],
	    	color_values[2],
	    );

        commands
	        .spawn(BodyBundle {
	            sprite: SpriteBundle {
			        texture: skin_texture.clone(),
			        sprite: Sprite {
			        	color: color,
			        	..default()
			        },
			        transform: Transform {
	                    translation: event.position.extend(1.0),
	                    scale: Vec3::new(
				        	SPRITE_SCALE, SPRITE_SCALE, 1.0
				        ),
				        rotation: Quat::IDENTITY,
	                    ..default()
	                },
			        ..default()
			    },

	            gene: GeneComponent(gene),
	            brain: BrainComponent(brain),

	            mass: default(),
	            momentum: default(),
	            heading: Heading(rng.gen_range(0.0..TAU)),
	            energy: Energy {
	            	amount: event.energy,
	            	spent: units::Energy(0.0),
	            },
	            space: Space(units::Energy(1.0)),
	            thickness: Thickness(1.0),
	            sensor: default(),
	            speed_sensor: default(),
	            clock: InnerClock {
	            	rate: 1.0,
	            	..default()
	            },
	            memory: default(),

	            move_control: default(),
	            action_control: default(),
	            antenna: default(),

	            child_count: default(),
	        })
	        .insert(Awake)
	        .with_children(|parent| {
	        	parent.spawn(BodyPartBundle {
			        part: BodyPart::Belly,
	        		sprite: SpriteBundle {
				        texture: belly_texture.clone(),
			            ..default()
			        },
			    });
	        	parent.spawn(BodyPartBundle {
			        part: BodyPart::Clock,
			        sprite: SpriteBundle {
				        texture: clock_texture.clone(),
			            ..default()
			        }
		        });
	        	parent.spawn(BodyPartBundle {
			        part: BodyPart::Antenna,
	        		sprite: SpriteBundle {
				        texture: antenna_texture.clone(),
			            ..default()
			        },
			    });
	        });
    }
}

fn read_sensors (
	map: Res<Map>,
	mut query: Query<(&mut Sensor, &Transform, &GeneComponent, &AntennaControl), With<Awake>>
) {
    for (mut sensor, transform, gene, antenna) in &mut query {
    	let atom = map.read(get_antenna_position(antenna, transform));
    	sensor.magnitude = atom.mag;
    	sensor.affinity = gene.0.skin.affinity(atom);
    }
}

fn read_speed_sensor (
	mut query: Query<(&mut SpeedSensor, &Heading, &Momentum), With<Awake>>
) {
    for (mut speed_sensor, heading, momentum) in &mut query {
    	let local_momentum = Vec2::from_angle(heading.0).rotate(momentum.0);
    	speed_sensor.magnitude = local_momentum.x;
    }
}

fn think (
	mut query: Query<(
		&Sensor, &SpeedSensor, &Energy, &Thickness, &InnerClock,
		&mut MoveControl, &mut ActionControl, &mut AntennaControl,
		&mut BrainComponent, &mut MemoryComponent,
	), With<Awake>>
) {
	let mut rng = thread_rng();
    for (
    	sensor, speed_sensor, energy, thickness, clock,
    	mut control, mut action, mut antenna,
    	mut brain, mut memory,
    ) in &mut query {
    	let output = brain.0.process(BrainInput {
    		age: clock.age,
    		energy: energy.amount.0,
    		thickness: thickness.0,
    		antenna_strength: sensor.magnitude,
    		antenna_affinity: sensor.affinity,
    		clock_sin: clock.phase.sin(),
    		clock_cos: clock.phase.cos(),
    		speed: speed_sensor.magnitude,
    	});

    	control.angle = output.angle;
    	control.force.0 = output.force;

    	antenna.angle = output.antenna_angle;
    	antenna.length = output.antenna_length;
    	antenna.size = output.antenna_size;

    	action.eat = output.eat;
    	action.deposit = output.deposit;
    	action.birth = output.split;

    	action.heal = output.heal;
    	action.metabolism = output.clock_rate;

    	if !output.angle.is_finite() {
    		brain.0.debug();
    	}
    }
}

fn update_sprites (
	mut query: Query<(
		Entity, &Children, &mut Sprite,
		&InnerClock, &Energy, &Thickness,
		&AntennaControl
	)>,
	mut child_query: Query<(&BodyPart, &mut Transform)>
) {
	const PI: f32 = 3.141592;
    for (
    	entity, children, mut sprite,
    	clock, energy, thickness,
    	antenna
    ) in &mut query {
    	for child in children {
    		if let Ok((body_part, mut transform)) = child_query.get_mut(*child) {
	    		match body_part {
	    			BodyPart::Clock => {
	    				transform.translation.y = clock.phase.sin() * 2.0;
	    			},
	    			BodyPart::Antenna => {
	    				let rot = Vec2::from_angle(PI - antenna.angle) * antenna.length * 32.0;
	    				transform.translation = rot.extend(0.0);
	    			},
	    			BodyPart::Belly => {
	    				let radius = energy.compute_mass().sqrt() * 0.5;
	    				transform.scale = Vec3::splat(radius);
	    			}
	    		}
	    	}
    	}

    	sprite.color.set_alpha(thickness.0.min(1.0));
    }
}

fn run_inner_clock (mut query: Query<(&mut InnerClock,)>) {
    for (mut clock,) in &mut query {
    	clock.phase += clock.rate * DELTA_CLOCK;
    	if clock.phase > TAU {
    		clock.phase -= TAU;
    	}

    	clock.age += DELTA_CLOCK;
    }
}

fn wake (
	mut commands: Commands,
	query: Query<(Entity, &InnerClock, &Energy)>
) {
    for (entity, clock, energy) in &query {
    	let wake = energy.amount.0 > 0.0;
    	if wake {
    		commands.entity(entity).insert(Awake);
    	} else {
    		commands.entity(entity).remove::<Awake>();
    	}
    }
}

fn update_mass (mut query: Query<(&mut Mass, &Energy, &Thickness)>) {
    for (mut mass, energy, thickness) in &mut query {
    	mass.0 = units::Mass(
    		// Base mass
    		units::Mass::BICHO.0 +

    		// Mass stored as fuel
    		energy.compute_mass() +

    		// Mass of the skin
    		thickness.0 * units::Mass::SKIN.0
    	);
    }
}

// TODO: Change momentum with the control in apply_action, and then change the transform with the momentum
fn apply_movement (
	map: Res<Map>,
	config: Res<crate::Config>,
	mut query: Query<(
		&mut Transform, &MoveControl, &mut Heading, &mut Momentum, &Mass
	), With<Awake>>
) {
	fn limit (value: &mut f32, size: f32) {
		let half = size * 0.5;
		*value = (*value + half).rem_euclid(size) - half;
	}

    let damping = (1.0 - config.friction).powf(DELTA);
    let size = map.size as f32;

    for (mut transform, control, mut heading, mut momentum, mass) in &mut query {
    	// Apply friction
    	momentum.0 *= damping;
    	let delta = DELTA / mass.0.0;

    	let force_dir = Vec2::new(0.0, 1.0).rotate(
    		Vec2::from_angle(heading.0)
    	);
    	momentum.0 += force_dir * control.force.0;

    	// Limit the speed
    	let speed = (momentum.0 * delta)
    		.max(Vec2::splat(-size*0.5))
    		.min(Vec2::splat(size*0.5));

        transform.translation += speed.extend(0.0);

        limit(&mut transform.translation.x, size);
        limit(&mut transform.translation.y, size);

        heading.0 += control.angle * units::Mass::FIN.0 * delta;

        transform.rotation = Quat::from_rotation_z(heading.0);
    }
}

fn react_antenna (
	mut map: ResMut<Map>,
	mut query: Query<(
		&Transform, &GeneComponent, &ActionControl, &AntennaControl, &mut Energy
	), With<Awake>>
) {
    for (
    	transform, gene, actions, antenna, mut energy,
    ) in &mut query {
    	if !actions.deposit && !actions.eat { return }

	    let skin = gene.0.skin;
	    let size = antenna.size;

		let antenna_pos = get_antenna_position(antenna, transform);
	    let mut atom = map.read(antenna_pos);

    	if actions.deposit {
    		let mag = size.min(energy.amount.0).max(0.0);
    		atom = react([atom, skin.with_magnitude(mag)]);
    		energy.amount.0 -= mag;
    	}

		if actions.eat {
	    	let eaten = size.min(atom.mag).max(0.0);
	    	atom.mag -= eaten;

	    	// Affinity scaled to -1..1
	    	let affinity = skin.affinity(atom) * 2.0 - 1.0;
	    	energy.amount.0 += eaten * affinity;
		}

	    map.write(antenna_pos, atom);
	}
}

fn react_skin (
	mut map: ResMut<Map>,
	mut query: Query<(&Transform, &InnerClock, &GeneComponent, &mut Thickness)>
) {
    for (transform, clock, gene, mut thickness) in &mut query {

    	// React with the map
    	let pos = transform.translation.truncate();

    	let mut skin_atom = gene.0.skin * thickness.0;
    	let skin_drop = {
    		let shed_rate = constants::SKIN_SHED * DELTA * (clock.age as f32);
    		skin_atom * shed_rate
    	};
    	skin_atom.mag -= skin_drop.mag;

    	let mut map_atom = map.read(pos);

    	if map_atom.mag > 0.0 {
	    	let map_drop = map_atom * (constants::SKIN_PERMEABILITY * DELTA);
	    	map_atom.mag -= map_drop.mag;

    		skin_atom = react([skin_atom, map_drop]);
    	}

    	map_atom = react([map_atom, skin_drop]);
    	map.write(pos, map_atom);

	    thickness.0 = skin_atom.mag;
	}
}

fn heal (mut query: Query<(&ActionControl, &mut Thickness), With<Awake>>) {
    for (actions, mut thickness) in &mut query {
    	if actions.heal >= 0.0 {
    		thickness.0 += actions.heal;
    	}
	}
}

/// Divide available energy evenly for all the actions the bicho wants to take,
/// and scale the actions accordingly
fn spend_energy (
	config: Res<crate::Config>,
	mut query: Query<(
		&Mass, &InnerClock, &mut Energy,
		&mut ActionControl, &mut AntennaControl, &mut MoveControl,
		&BrainComponent,
	), With<Awake>>
) {
    for (
    	mass, clock, mut energy,
    	mut actions, mut antenna, mut control,
    	brain,
    ) in &mut query {
    	let mut spent = 0.0;

    	let mut current_energy = energy.amount.0;

    	spent += constants::LIVING_COST * clock.age * DELTA;
    	spent += (control.force.0 * DELTA).abs();
    	spent += actions.heal.max(0.0);
    	spent += (antenna.length * units::Mass::ANTENNA.0 * DELTA).abs();
    	spent += brain.0.compute_energy_use(config.neuron_energy_use) * DELTA;
    	if actions.birth {
    		// For splitting, first request as much energy as we have.
    		spent += current_energy;
    	}

    	if spent > energy.amount.0 {
    		let scale = energy.amount.0 / spent;

    		spent *= scale;

	    	control.force.0 *= scale;
	    	actions.heal *= scale;
	    	antenna.length *= scale;
	    	current_energy *= scale;
    	}

    	energy.amount.0 -= spent;

    	// Because splitting takes half of all the energy, but we requested all of it,
    	// leave the bicho with half the energy allowed to spend in splitting.
    	// The life events system will duplicate the bicho with however much energy it has.
    	if actions.birth {
    		// If after energy allocation, there's not enough energy to split,
    		// it won't be spent.
    		// This means trying to split without enough energy makes other actions less efficient.
    		let cost = constants::BIRTH_COST;
    		if current_energy > cost {
    			energy.amount.0 = (current_energy - cost) / 2.0;
    		} else {
    			actions.birth = false;
    			energy.amount.0 = current_energy;
    		}
    	}
    }
}

fn self_regulate (
	mut query: Query<(&ActionControl, &mut InnerClock), With<Awake>>
) {
    for (actions, mut clock) in &mut query {
    	// TODO: Rename to clock_rate
    	clock.rate = actions.metabolism;

    	// TODO: Add sleeping.
    	// Bicho choses how many ticks to sleep for, and doesn't age
    	// or spend energy while sleeping. It does react though.
   	}
}

fn kill (
	mut commands: Commands,
    mut death_events: EventWriter<crate::deadpool::DeathEvent>,
	query: Query<(Entity, &Transform, &GeneComponent, &Energy, &Thickness, &InnerClock, &ChildCount)>
) {
	let eps = std::f32::EPSILON;

    for (entity, transform, gene, energy, thickness, clock, child_count) in &query {
    	// If a bicho looses its skin, it dies
    	if thickness.0 < eps || energy.amount.0 < eps {
    		death_events.send(crate::deadpool::DeathEvent {
    			gene: gene.0.clone(),
    			age: clock.age,
    			child_count: child_count.0,
    			position: transform.translation.truncate(),
    		});
    		commands.entity(entity).despawn_recursive();
    	}
    }
}

fn split (
    mut spawn_events: EventWriter<SpawnEvent>,
	mut query: Query<(
		&Transform, &GeneComponent, &ActionControl, &Energy, &mut ChildCount
	), With<Awake>>
) {
    for (transform, gene, actions, energy, mut child_count) in &mut query {
    	if actions.birth {
    		child_count.0 += 1;
	    	spawn_events.send(SpawnEvent {
	    		position: transform.translation.truncate(),
	    		energy: units::Energy(energy.amount.0),
	    		gene: gene.0.clone_mutated(),
	    	});
    	}

    }
}
