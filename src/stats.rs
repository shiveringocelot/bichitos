
use std::collections::VecDeque;

use bevy::{
	prelude::*,
	input::mouse::{
		MouseButtonInput,
		MouseButton::Left,
	},
	input::ButtonState::Pressed,
};

use bevy_egui::{
    EguiContextQuery
};

use crate::{
	world::{Map, Time},
	bicho::{self},
	constants, units,
};


mod components {
	use super::*;

	#[derive(Component)]
	pub enum BichoLabel {
		Health,
		Energy,
	}

	#[derive(Component)]
	pub struct BichoPanel;

	#[derive(Component)]
	pub struct BrainPanel;
}

use components::*;

#[derive(Default, Clone)]
pub struct Sample {
    pub bicho_count: u32,
    pub total_mass: f32,
    pub average_mass: f32,
    pub average_age: f32,
}

impl Sample {
    fn combine (self, other: Sample) -> Sample {
        Sample {
            bicho_count: (self.bicho_count + other.bicho_count) / 2,
            total_mass: (self.total_mass + other.total_mass) / 2.0,
            average_mass: (self.average_mass + other.average_mass) / 2.0,
            average_age: (self.average_age + other.average_age) / 2.0,
        }
    }
}


#[derive(Default)]
pub struct History {
    samples: VecDeque<Sample>,
    next: Option<Box<History>>,

    // This alternates every new sample
    queued: Option<Sample>,
}

#[derive(Resource, Default)]
pub struct Stats {
    pub current: Sample,
	pub max: Sample,

    pub history: History,
}

impl History {
    fn push (&mut self, sample: Sample) {
        let capacity = constants::HISTORY_FRAME_SIZE;

        if self.samples.len() >= capacity {
            let front = self.samples.pop_front().unwrap();

            match self.queued.clone() {
                Some(queued) => {
                    if self.next.is_none() {
                        self.next = Some(Box::new(default()));
                    }

                    let avg = front.combine(queued);
                    self.next.as_mut().unwrap().push(avg);

                    self.queued = None;
                },
                None => {
                    self.queued = Some(front);
                }
            }
        }

        self.samples.push_back(sample);
    }

    pub fn iter_samples (&self) -> Box<dyn Iterator<Item=&Sample> + '_> {
        let self_iter = self.samples.iter().rev();
        match self.next.as_ref() {
            Some(next) => Box::new(self_iter.chain(next.iter_samples())),
            None => Box::new(self_iter),
        }
    }
}

fn update (
    mut stats: ResMut<Stats>,
    map: Res<Map>,
    time: Res<Time>,
    query: Query<(&bicho::components::InnerClock, &bicho::components::Mass)>
) {
	if time.is_generation_start() {
        let current = stats.current.clone();
        stats.history.push(current);

		let cell_mass: f32 = map.iter_cells().map(|cell| cell.mag).sum();

		let mut count = 0;
        let mut bicho_mass = 0.0;
		let mut age = 0.0;
	    for (clock, mass) in &query {
	    	count += 1;
	    	bicho_mass += mass.0.0;
            age += clock.age;
	    }

        stats.current.total_mass = cell_mass + bicho_mass;
        stats.current.bicho_count = count;
        if count > 0 {
    	    stats.current.average_mass = bicho_mass / count as f32;
    	    stats.current.average_age = age / count as f32;
        } else {
            stats.current.average_mass = 0.0;
            stats.current.average_age = 0.0;
        }

        stats.max.total_mass = stats.max.total_mass.max(stats.current.total_mass);
        stats.max.bicho_count = stats.max.bicho_count.max(stats.current.bicho_count);
        stats.max.average_mass = stats.max.average_mass.max(stats.current.average_mass);
        stats.max.average_age = stats.max.average_age.max(stats.current.average_age);
	}
}

// TODO: Move to gui
#[derive(Component)]
pub struct Focus;

#[derive(Default, Clone, Event)]
struct ClickEvent (Vec2);

fn click_event_listener (
	mut commands: Commands,
    map: Res<Map>,
    mut events: EventReader<ClickEvent>,
    query: Query<
    	(Entity, &Transform, &bicho::components::BrainComponent),
    	Without<Focus>
    >,
    mut focus_q: Query<Entity, With<Focus>>,
    mut brain_panel_q: Query<Entity, With<BrainPanel>>,
    mut egui_q: Query<EguiContextQuery>
) {
    let egui_lock = egui_q.iter_mut().any(|mut ctx| {
        let egui_ctx = ctx.ctx.get_mut();
        egui_ctx.wants_pointer_input()
            || egui_ctx.wants_keyboard_input()
            || egui_ctx.is_pointer_over_area()
    });

    if egui_lock { return; }

    if let Some(event) = events.read().last() {
    	let mut closest = None;
    	for (entity, transform, brain) in &query {
    		let dist = transform.translation.truncate().distance(event.0);
    		if dist > 1.0 { continue }

    		let replace;
    		if let Some((closest_dist, _)) = closest {
    			replace = dist < closest_dist;
    		} else {
    			replace = true;
    		}

    		if replace {
    			closest = Some((dist, (entity, brain)));
    		}
    	}

		for focus_entity in focus_q.iter() {
			commands.entity(focus_entity).remove::<Focus>();
		}

    	if let Some((_, (entity, brain))) = closest {
    		commands.entity(entity).insert(Focus);

    		/*commands.entity(brain_panel_q.single())
    		.with_children(|parent| {

    		});*/
    	}

	}
}

fn check_click (
    camera_q: Query<(&Camera, &GlobalTransform)>,
    window_q: Query<(&Window)>,
    mut mouse_button_input_events: EventReader<MouseButtonInput>,
    mut click_events: EventWriter<ClickEvent>,
) {
    for event in mouse_button_input_events.read() {
    	if event.button == Left && event.state == Pressed {
			// get the camera info and transform
		    // assuming there is exactly one main camera entity, so query::single() is OK
		    let (camera, camera_transform) = camera_q.single();
		    let (window) = window_q.single();

		    // check if the cursor is inside the window and get its position
		    // then, ask bevy to convert into world coordinates, and truncate to discard Z
		    if let Some(world_position) = window.cursor_position()
		        .and_then(|cursor| camera.viewport_to_world(camera_transform, cursor))
		        .map(|ray| ray.origin.truncate())
		    {
		    	click_events.send(ClickEvent(world_position));
		        //eprintln!("World coords: {}/{}", world_position.x, world_position.y);
		    }
    	}
    }
}

fn follow_focus (
    mut camera_q: Query<&mut Transform, (With<Camera>, Without<Focus>)>,
    query: Query<&Transform, (With<Focus>, Without<Camera>)>,
) {
	if let Some(transform) = query.iter().last() {
		let mut camera_transform = camera_q.single_mut();

        const MAX: f32 = constants::MAP_SIZE as f32;
		camera_transform.translation = transform.translation
            .min(Vec3::splat(MAX))
            .max(Vec3::splat(-MAX));
	}
}

fn update_bicho_hud(
	focus_q: Query<(
        &Transform,
		&bicho::components::Energy,
	), With<Focus>>,
	mut panel_q: Query<&mut Visibility, With<BichoPanel>>,
	mut label_q: Query<(&mut Text, &BichoLabel)>,
) {
	if let Some((transform, energy)) = focus_q.iter().next() {
		for mut panel_visibility in &mut panel_q {
			*panel_visibility = Visibility::Visible;
		}

		for (mut text, label) in &mut label_q {
			let value = match label {
				BichoLabel::Health => format!(
                    "Pos: {:.2},{:.2}",
                    transform.translation.x,
                    transform.translation.y
                ),
				BichoLabel::Energy => format!("Energy: {:.2}", energy.amount.0),
			};

			text.sections[0].value = value.into();
		}
	} else {
		for mut panel_visibility in &mut panel_q {
			*panel_visibility = Visibility::Hidden;
		}
	}
}

fn setup_ui(
	mut commands: Commands,
	asset_server: Res<AssetServer>
) {
    commands
    .spawn(NodeBundle {
        style: Style {
            width: Val::Percent(100.0),
            justify_content: JustifyContent::SpaceBetween,
            ..default()
        },
        ..default()
    })
    .with_children(|parent| {
        // left vertical fill (border)
        parent
        .spawn((
        	NodeBundle {
                style: Style {
                    width: Val::Px(200.0),
                    border: UiRect::all(Val::Px(2.0)),
                    flex_direction: FlexDirection::Column,
                    ..default()
                },
                background_color: Color::rgba(0.3, 0.3, 0.3, 0.3).into(),
                ..default()
            },
            BichoPanel,
        ))
        .with_children(|parent| {
            // text
            parent.spawn((
                TextBundle::from_section(
                    "Bicho",
                    TextStyle {
                        font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                        font_size: 30.0,
                        color: Color::WHITE,
                    },
                )
                .with_style(Style {
                    margin: UiRect::all(Val::Px(5.0)),
                    ..default()
                }),
                // Because this is a distinct label widget and
                // not button/list item text, this is necessary
                // for accessibility to treat the text accordingly.
                Label,
            ));

            let base_bundle = TextBundle::from_section(
                "", TextStyle {
                    font: asset_server.load("fonts/FiraMono-Medium.ttf"),
                    color: Color::WHITE,
                    ..default()
                },
            ).with_style(Style {
                margin: UiRect::all(Val::Px(5.0)),
                ..default()
            });

            // TODO: TextBundle no longer implements clone??? bruh
            //parent.spawn((base_bundle.clone(), Label, BichoLabel::Health));
            //parent.spawn((base_bundle.clone(), Label, BichoLabel::Energy));

            // Brain Panel
            parent.spawn((
            	NodeBundle {
	                ..default()
            	},
            	BrainPanel,
            ));
        });
    });
}

pub struct StatsPlugin;
impl Plugin for StatsPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Stats::default())
            .add_event::<ClickEvent>()
            .add_systems(Startup, setup_ui)
            .add_systems(Update, (
                check_click,
                click_event_listener,
                follow_focus,
                update_bicho_hud,
                update,
            ));
    }
}

