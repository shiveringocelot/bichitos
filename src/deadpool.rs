
// The deadpool is an artificially bred pool of bicho genes.
// It evolves more efficiently, but it's only used to recover dead populations.
// As the deadpool spawn rate is low, as soon as a successful species evolve,
// it will overflow the deadpool with its genes and outpace its evolution.

use bevy::{
    prelude::*,
    input::mouse::{
        MouseButtonInput,
        MouseButton::Left,
    },
    input::ButtonState::Pressed,
};

use crate::{
    constants, math,
    gene::{Gene, GeneData},
};

use rand::{thread_rng, Rng};

#[derive(Component, Default, Clone)]
pub struct Pool {
    // Use the samples capacity
    samples: Vec<Sample>,
}

impl Pool {
    pub fn new () -> Self { Self {
        samples: Vec::with_capacity(constants::DEADPOOL_CAPACITY)
    } }
    pub fn is_empty (&self) -> bool { self.samples.is_empty() }
    pub fn sample (&self) -> &Gene {
        let mut rng = thread_rng();
        &self.samples[rng.gen_range(0..self.samples.len())].gene
    }

    pub fn add_sample (&mut self, gene: &Gene, fitness: f32, rng: &mut impl Rng) {
        let capacity = self.samples.capacity();

        let i = rng.gen_range(0..capacity);

        if i >= self.samples.len() {
            self.samples.push(Sample {
                fitness, gene: gene.clone(),
            });
        } else {
            let competitor = &self.samples[i];
            let lucky = rng.gen_bool(constants::DEADPOOL_LUCKY_RATE as f64);

            if lucky || fitness > competitor.fitness {
                self.samples[i] = Sample {
                    fitness, gene: gene.clone(),
                };
            }
        }
    }

    /// Encodes this Dead Pool into a vector of tuples suitable for storage by [crate::store]
    pub fn get_store_data (&self) -> Vec<(f32, GeneData)> {   
        self.samples.iter().map(|sample| (sample.fitness, (&sample.gene).into())).collect()
    }

    /// Encodes this Dead Pool into a vector of tuples suitable for storage by [crate::store]
    pub fn from_store_data (data: Vec<(f32, GeneData)>) -> Pool {   
        Pool { samples: data.into_iter().map(
            |(fitness, gene_data)| Sample {
                fitness,
                gene: gene_data.into()
            }
        ).collect() }
    }
}

#[derive(Clone)]
struct Sample {
    gene: Gene,
    fitness: f32,
}

#[derive(Event)]
pub struct DeathEvent {
    pub gene: Gene,
    pub age: f32,
    pub child_count: u32,
    pub position: Vec2,
}

/*
fn death_event_listener (
    mut commands: Commands,
    mut pool: ResMut<Pool>,
    mut events: EventReader<DeathEvent>,
) {
    let mut rng = thread_rng();

    for event in events.iter() {
        let fitness = math::sigmoid(event.age) + math::sigmoid(event.child_count as f32) * 0.5;
        let capacity = pool.samples.capacity();

        let i = rng.gen_range(0..capacity);

        if i >= pool.samples.len() {
            pool.samples.push(Sample {
                fitness, gene: event.gene.clone(),
            });
        } else {
            let competitor = &pool.samples[i];
            let lucky = rng.gen_bool(constants::DEADPOOL_LUCKY_RATE as f64);

            if lucky || fitness > competitor.fitness {
                pool.samples[i] = Sample {
                    fitness, gene: event.gene.clone(),
                };
            }
        }
    }
}
*/

pub struct Plugin;
impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut App) {
        app.add_event::<DeathEvent>();
    }
}

