
use bevy::{
	prelude::*,
    math::vec2,
	render::render_resource::{
		Extent3d, TextureDimension, TextureFormat
	},
};

use rand::{thread_rng, Rng};
use crate::{
    constants,
    chemistry::{Atom, react},
};


#[derive(Resource, Default)]
pub struct Time(u32);
impl Time {
    fn tick (&mut self) {
        self.0 += 1;
    }

    pub fn is_generation_start (&self) -> bool {
        return self.0 % constants::TICKS_PER_GENERATION == 0;
    }
}

#[derive(Resource)]
pub struct Map {
    pub size: usize,
    cells: Vec<Atom>,
    inactive_cells: Vec<Atom>,
}

#[derive(Component)]
struct MapComponent;

impl Map {
    fn new (size: usize) -> Map {
        let cells = vec![Atom::zero(); size*size];
        return Map { size, inactive_cells: cells.clone(), cells }
    }

    fn swap_maps (&mut self) {
        std::mem::swap(&mut self.cells, &mut self.inactive_cells);
    }

    pub fn image_bytes (&self) -> Vec<u8> {
    	let mut bytes = Vec::with_capacity(self.size.pow(2) * 4);

    	for y in 0..self.size {
    		for x in 0..self.size {
    			let index = y * self.size + x;
                let color = self.cells[index].get_color();

                fn to_byte (f: f32) -> u8 {
                    (f * 256.0) as u8
                }

                bytes.extend(&[
                    to_byte(color[0]),
                    to_byte(color[1]),
                    to_byte(color[2]),
                    255
                ]);

    			/*let zero_bytes = 0f32.to_le_bytes();
    			let val_bytes = cell.brightness().to_le_bytes();
    			bytes.extend(zero_bytes);
    			bytes.extend(val_bytes);
    			bytes.extend(zero_bytes);
    			bytes.extend(1.0f32.to_le_bytes());
                */
    		}
    	}

    	return bytes;
    }

    fn get_position_index (&self, pos: Vec2) -> usize {
        fn int (x: f32, size: usize) -> usize {
            let s = size as i32;
            let shifted = x as i32 + s/2 - 1;
            let wrapped = shifted.rem_euclid(s);
            wrapped as usize
        }

        assert!(pos.x.is_finite());
        assert!(pos.y.is_finite());

        let s = self.size;
        let x = int(pos.x, s);
        let y = int(-pos.y, s);

        y*s + x
    }

    pub fn read_xy (&self, x: u32, y: u32) -> Atom {
        // TODO: this should be the base function for read_vec2
        let index = self.get_position_index(Vec2::new(x as f32, y as f32));
        return self.cells[index];
    }

    pub fn read (&self, pos: Vec2) -> Atom {
        let index = self.get_position_index(pos);
        return self.cells[index];
    }

    pub fn write (&mut self, pos: Vec2, value: Atom) {
        let index = self.get_position_index(pos);
        self.cells[index] = value;
    }

    pub fn add (&mut self, pos: Vec2, add: Atom) {
        let index = self.get_position_index(pos);
        let cell = self.cells[index];

        self.cells[index] = react([cell, add]);
    }

    pub fn iter_cells (&self) -> impl Iterator<Item=&Atom> {
        return self.cells.iter();
    }

    pub fn compute_mass (&self) -> f32 {
        return self.iter_cells().map(|c| c.mag).sum();
    }

    pub fn get_cells_ref (&self) -> &[Atom] {
        &self.cells
    }

    pub fn set_cells (&mut self, cells: Vec<Atom>) {
        if cells.len() != self.cells.len() {
            panic!("Invalid cell count: map has {} cells, but {} cells were set", self.cells.len(), cells.len());
        }
        self.cells = cells;
    }
}

fn tick_time(
    mut time: ResMut<Time>, 
) {
    time.tick();
}

fn diffuse(
    config: Res<crate::config::Config>,
    time: ResMut<Time>,
    mut map: ResMut<Map>,
) {
    if time.is_generation_start() {

        let diff = config.cell_spread_rate;
        let remain = 1.0 - config.cell_decay_rate - diff;

        let s = map.size;
        let scale = diff * 0.25;

        for y in 0..s {
            for x in 0..s {

                let i = y * s + x;
                let i_x1 = y * s + (x+1)%s;
                let i_x2 = y * s + (x+s-1)%s;
                let i_y1 = ((y+1)%s) * s + x;
                let i_y2 = ((y+s-1)%s) * s + x;

                let cell = map.cells[i];

                let east = map.cells[i_x1] * scale;
                let west = map.cells[i_x2] * scale;
                let north = map.cells[i_y1] * scale;
                let south = map.cells[i_y2] * scale;

                /*let reaction = cell.react(east)
                    .combine(cell.react(west))
                    .combine(cell.react(north))
                    .combine(cell.react(south))
                ;

                let mix = (cell * remain).apply(reaction);*/
                let mix = react([cell * remain, east, west, north, south]);
                map.inactive_cells[i] = mix;
            }
        }

        map.swap_maps();
    }
}


pub struct WorldPlugin;
impl Plugin for WorldPlugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Map::new(constants::MAP_SIZE))
            .insert_resource(Time::default())
            .add_systems(Update, (tick_time, diffuse));
    }
}

mod tests {
    use super::*;

    #[test]
    fn can_read() {
        const MAP_SIZE: usize = 16;

        let mut map = Map::new(MAP_SIZE);

        let mut rng =  thread_rng();
        let mut values = vec![];

        for y in 0..MAP_SIZE {
            for x in 0..MAP_SIZE {
                let val = 1.0 / rng.gen_range(0.1..1.0) - 1.0;

                map.write(Vec2::new(x as f32, y as f32), Atom::zero().with_magnitude(val));
                values.push(val);
            }
        }

        for y in 0..MAP_SIZE {
            for x in 0..MAP_SIZE {
                assert_eq!(
                    map.read(Vec2::new(x as f32, y as f32)).mag,
                    values.remove(0)
                );
            }
        }
    }
}
