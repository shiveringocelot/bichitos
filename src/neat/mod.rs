
// Implementation of the NEAT Algorithm

use rand::{thread_rng, Rng};
use topological_sort::TopologicalSort;

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct Genome {
    pub input_count: usize,
    pub output_count: usize,
    pub nodes: Vec<Node>,
    pub connections: Vec<Connection>,

    // Used mostly for network construction
    pub layers: Vec<LayerInfo>,
}

#[derive(Clone, Copy, serde::Serialize, serde::Deserialize)]
pub enum Node {
    Input, Output,
    LeakyRelu, Sigmoid,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct Connection {
    pub input: usize,
    pub output: usize,
    pub weight: f32,
    pub enabled: bool,
    pub innovation: usize,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct LayerInfo {
    pub level: usize,
    pub nodes: Vec<usize>,
}

impl Genome {
    pub fn create_initial (input_count: usize, output_count: usize) -> Genome {
        let in_nodes = (0..input_count).map(|_| Node::Input);
        let out_nodes = (0..output_count).map(|_| Node::Output);

        let mut genome = Genome {
            input_count, output_count,
            nodes: in_nodes.chain(out_nodes).collect(),
            connections: (0..output_count).map(move |output|
                (0..input_count).map(move |input| Connection {
                    input: input,
                    output: output + input_count,
                    weight: 0.0,
                    enabled: true,
                    innovation: output*output_count + input,
                })
            ).flatten().collect(),
            layers: vec![],
        };
        genome.compute_layers();
        genome
    }

    pub fn split_connection (&mut self, index: usize) {
        let connection = self.connections[index].clone();

        let split_index = self.nodes.len();
        self.nodes.push(Node::LeakyRelu);

        self.add_connection(connection.input, split_index, 1.0);
        self.add_connection(split_index, connection.output, connection.weight);
        self.connections[index].enabled = false;
    }

    pub fn add_connection (&mut self, input: usize, output: usize, weight: f32) {
        let innov = self.connections.len();
        self.connections.push(Connection {
            weight, input, output,
            enabled: true,
            innovation: innov,
        });
    }

    pub fn apply_mutations (&mut self) {
        self.compute_layers();
    }

    fn compute_layers (&mut self) {
        let mut ts = TopologicalSort::<usize>::new();

        for connection in &self.connections {
            if (connection.enabled) {
                ts.add_dependency(connection.input, connection.output)
            }
        }

        self.layers.clear();
        let mut level = 0;
        while let layer_nodes = ts.pop_all() {
            if layer_nodes.is_empty() { break; }
            self.layers.push(LayerInfo { level, nodes: layer_nodes });
            level = level+1;
        }
    }

    pub fn to_network (&self) -> Network {

        // Map going fro genome index to perceptron-topological index
        let mut index_map = vec![None; self.nodes.len()];
        let mut nodes = vec![];

        fn to_activation (node: Node) -> Activation {
            match node {
                Node::LeakyRelu => Activation::LeakyRelu,
                _ => Activation::None,
            }
        }

        // Add the nodes in topological order
        for layer in &self.layers {
            for index in &layer.nodes {
                index_map[*index] = Some(nodes.len());
                nodes.push(Perceptron {
                    id: nodes.len(),
                    level: layer.level,
                    activation: to_activation(self.nodes[*index]),
                    inputs: vec![],
                });
            }
        }

        // Read the connetions to the network nodes
        for conn in &self.connections {
            if !conn.enabled { continue }

            if let (Some(input), Some(output)) =
                (index_map[conn.input], index_map[conn.output]) {
                nodes[output].inputs.push(Synapse {
                    input, weight: conn.weight
                })
            }
        }

        Network {
            nodes,
            inputs: (0 .. self.input_count).map(|i|
                index_map[i]
            ).collect(),
            outputs: (0 .. self.output_count).map(|i|
                index_map[i + self.input_count]
            ).collect(),
        }
    }
}



#[derive(Clone)]
pub struct Network {
    pub inputs: Vec<Option<usize>>,
    pub outputs: Vec<Option<usize>>,
    pub nodes: Vec<Perceptron>,
}

#[derive(Clone)]
pub struct Perceptron {
    pub id: usize,
    pub level: usize,
    pub activation: Activation,
    pub inputs: Vec<Synapse>,
}

#[derive(Clone)]
pub struct Synapse {
    pub input: usize,
    pub weight: f32,
}

#[derive(Clone, Copy)]
pub enum Activation { None, LeakyRelu, Sigmoid }

impl Activation {
    fn apply (self, x: f32) -> f32 {
        match self {
            Activation::None => x,
            Activation::LeakyRelu =>
                if x >= 0.0 { x } else { x*0.01 },
            Activation::Sigmoid => {
                // TODO: Fastmoid?
                1.0 / ((-x).exp() + 1.0)
            }
        }
    }
}

impl Network {
    pub fn size (&self) -> usize { self.nodes.len() }
    pub fn forward (&self, values: &mut [f32]) {
        for (i, perceptron) in self.nodes.iter().enumerate() {
            // Don't process source nodes
            if !perceptron.inputs.is_empty() {
                let sum = perceptron
                    .inputs.iter()
                    .map(|synapse| values[synapse.input] * synapse.weight)
                    .sum();
                values[i] = perceptron.activation.apply(sum);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn network_1_1 () {
        let mut network = Network {
            inputs: vec![Some(0)],
            outputs: vec![Some(1)],
            nodes: vec![
                Perceptron { id: 0, level: 0, inputs: vec![], activation: Activation::None },
                Perceptron { id: 1, level: 1, activation: Activation::LeakyRelu, inputs: vec![
                    Synapse { input: 0, weight: 0.5 }
                ] },
            ],
        };

        let mut values = vec![0.0; network.size()];

        values[0] = 1.5;
        network.forward(&mut values);
        assert_eq!(values[1], 0.75);
    }
}