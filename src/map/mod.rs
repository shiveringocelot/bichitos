// This module is in charge of rendering and displaying the cells.
// Unlike the world module, this does not deal with how cells are represented and evolve.

use bevy::{
    prelude::*,
    sprite::{
        MaterialMesh2dBundle,
        Material2d,
        Material2dPlugin
    },
    render::{
        extract_resource::{
            ExtractResource, ExtractResourcePlugin,
        },
        render_asset::RenderAssets,
        render_asset::RenderAssetUsages,
        render_graph::{self, RenderGraph},
        render_resource::*,
        renderer::{
            RenderContext, RenderDevice, RenderQueue,
        },
        texture::GpuImage,
        Render, RenderApp, RenderSet,
    },
};


fn setup(
    mut commands: Commands,
    map: Res<crate::world::Map>,
    mut images: ResMut<Assets<Image>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<CustomMaterial>>,
) {
    // Start with an empty image
    let mut image = Image::new_fill(
        Extent3d {
            width: map.size as u32,
            height: map.size as u32,
            depth_or_array_layers: 1,
        },
        TextureDimension::D2,
        &[0, 0, 0, 255],
        TextureFormat::Rgba8Unorm,
        // TODO: What is this parameter?
        RenderAssetUsages::RENDER_WORLD,
    );
    image.texture_descriptor.usage = TextureUsages::COPY_DST
        | TextureUsages::STORAGE_BINDING
        | TextureUsages::TEXTURE_BINDING;
    let image = images.add(image);

    commands.spawn(MaterialMesh2dBundle {
        mesh: meshes
            .add(Rectangle::new(map.size as f32, map.size as f32))
            .into(),
        material: materials.add(CustomMaterial {
            texture: image.clone()
        }),
        transform: Transform::default(),
        ..default()
    });

    commands.insert_resource(CustomResource(image));
}


pub struct Plugin;
impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup)
            .add_plugins(Material2dPlugin::<CustomMaterial>::default())
            .add_plugins(ExtractResourcePlugin::<CustomResource>::default())
            .add_plugins(ExtractResourcePlugin::<ExtractedMap>::default());

        let render_app = app.sub_app_mut(RenderApp);
        render_app
            .add_systems(Render, prepare_custom.in_set(RenderSet::Prepare));
    }
}



#[derive(Asset, TypePath, AsBindGroup, Debug, Clone)]
struct CustomMaterial {
    #[texture(0)]
    #[sampler(1)]
    texture: Handle<Image>,
}

impl Material2d for CustomMaterial {
    fn fragment_shader() -> ShaderRef {
        "map.wgsl".into()
    }
}

#[derive(Resource, Clone, Deref, ExtractResource)]
struct CustomResource(Handle<Image>);

#[derive(Resource, Default)]
struct ExtractedMap {
    dimensions: (u32, u32),
    image_data: Vec<u8>,
}

impl ExtractResource for ExtractedMap {
    type Source = crate::world::Map;

    fn extract_resource(map: &Self::Source) -> Self {
        ExtractedMap {
            dimensions: (map.size as u32, map.size as u32),
            image_data: map.image_bytes(),
        }
    }
}

// write the extracted time into the corresponding uniform buffer
fn prepare_custom(
    custom: Res<CustomResource>,
    gpu_images: Res<RenderAssets<GpuImage>>,
    map: Res<ExtractedMap>,
    render_queue: Res<RenderQueue>,
    render_device: Res<RenderDevice>,
) {
    let size = 4 * map.dimensions.0 * map.dimensions.1;
    let image = gpu_images.get(&custom.0).unwrap();

    render_queue.write_texture(
        ImageCopyTexture {
            texture: &image.texture,
            mip_level: 0,
            origin: Origin3d::ZERO,
            aspect: TextureAspect::All,
        },
        &map.image_data,
        ImageDataLayout {
            offset: 0,
            bytes_per_row: Some(4 * map.dimensions.0),
            rows_per_image: Some(map.dimensions.1),
        },
        Extent3d {
            width: map.dimensions.0,
            height: map.dimensions.1,
            depth_or_array_layers: 1,
        },
    );
}