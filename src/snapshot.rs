
use bevy::{
    prelude::*,
};

pub struct SnapshotRequest;

pub struct Snapshot {
    pub cells: Vec<CellSnapshot>,
    pub deadpool: Vec<DeadpoolSampleSnapshot>,
    pub bichos: Vec<BichoSnapshot>,
}

pub struct Plugin;
impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup)
            .add_event::<SnapshotRequest>()
            .add_systems(Update, request_listener);
    }
}

fn request_listener(
    mut commands: Commands,
    mut spawn_events: EventReader<SpawnEvent>
) {
}