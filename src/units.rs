
use std::ops::{Add};

/// Measured in picograms.
#[derive(Default, Clone, Copy)]
pub struct Mass (pub f32);
impl Mass {
	/// 1 picogram
	/// [Typical mass of a bacteria](https://hypertextbook.com/facts/2003/LouisSiu.shtml).
	pub const BICHO: Mass = Mass(1.0);
	pub const ANTENNA: Mass = Mass(0.01);
	pub const FIN: Mass = Mass(0.01);
	pub const SKIN: Mass = Mass(0.5);
	pub const BELLY: Mass = Mass(0.5);
}


/// Measured in ... (TODO: use a unit lol)
#[derive(Default, Clone, Copy)]
pub struct Energy (pub f32);
pub type Force = Energy;

impl Energy {
    fn compute_mass (self, density: EnergyDensity) -> Mass {
        Mass(self.0 * density.0)
    }
}


/// Measured in [energy] per picogram (TODO: Use a unit here also)
#[derive(Default, Clone, Copy)]
pub struct EnergyDensity (pub f32);
impl EnergyDensity {
    pub const BICHO_FUEL: EnergyDensity = EnergyDensity(1.0);
}


/// Measured in micrometers.
///
/// [Bacteria size comparison](https://commons.wikimedia.org/wiki/File:Shapes_of_bacteria_and_size_comparisons.jpg).
#[derive(Default, Clone, Copy)]
pub struct Length (pub f32);


/// Measured in seconds
pub struct Duration (pub f32);

impl Duration {
	/// Duration of a generation
	pub const GENERATION: Duration = Duration(1.0);

	/// Duration of a tick
	pub const TICK: Duration = Duration(
		Self::GENERATION.0 / crate::constants::TICKS_PER_GENERATION as f32
	);
}

