use std::thread;
use std::sync::{Mutex, mpsc};
use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

use libui::prelude::*;
use libui::controls::{
    Control, Button, Group, Label, Spinbox, Entry,
    VerticalBox, HorizontalBox,
    LayoutGrid, GridAlignment, GridExpand,
    Area, AreaDrawParams, AreaHandler,
};

use bevy::{
    prelude::*,
    input::mouse::{
        MouseButtonInput,
        MouseButton::Left,
    },
    input::ButtonState::Pressed,

};

use crate::{
    config::Config,
    brain::BrainDraw,
    stats::{Focus, Stats},
    store::{SaveEvent, LoadEvent},
    GameState, bicho,
};

#[derive(Debug, Clone)]
pub enum GuiEvent {
    Pause, Quit, Save(Option<String>), Load(Option<String>),

    ConfigChange(String, f32),
}
unsafe impl Send for GuiEvent {}

#[derive(Default, Debug, Clone)]
pub struct Dict {
    entries: Vec<(String, f32)>,
}

#[derive(Debug, Clone)]
pub enum AppEvent {
    Ping,
    WorldStats(Dict),
    BichoStats(Dict),
    ConfigUpdate(Dict),
    Brain(Option<BrainDraw>),
}
unsafe impl Send for AppEvent {}

// Are resources with mutexes an acceptable practive in bevy?
// I'm trying not to lock anyways, not in the game loop.
#[derive(Resource)]
pub struct GuiResource {
    pub gui_rx: Mutex<mpsc::Receiver<GuiEvent>>,
    pub app_tx: Mutex<mpsc::Sender<AppEvent>>,
}

struct BrainDrawHandle {
    brain_draw: Rc<RefCell<Option<BrainDraw>>>,
}
impl AreaHandler for BrainDrawHandle {
    fn draw(&mut self, _area: &Area, draw_params: &AreaDrawParams) {
        use std::f64::consts::PI;
        use libui::draw::{Brush, FillMode, Path, SolidBrush, StrokeParams};

        let ctx = &draw_params.context;

        fn screen_pos ((x, y): (u32, u32), (w, h): (f64, f64)) -> (f64, f64) {
            ((x+1) as f64 * w, (y+1) as f64 * h)
        }

        fn value_color (val: f32) -> SolidBrush {
            let color_value = val.abs().min(1.0) as f64;
            if val < 0.0 {
                SolidBrush {
                    r: color_value,
                    g: 0.,
                    b: 0.,
                    a: 1.,
                }
            } else {
                SolidBrush {
                    r: 0.,
                    g: color_value,
                    b: 0.,
                    a: 1.,
                }
            }
        }

        let bg_brush = Brush::Solid(SolidBrush {
            r: 0.05,
            g: 0.,
            b: 0.1,
            a: 0.,
        });

        let synapse_stroke = StrokeParams {
            cap: 0,
            join: 0,
            thickness: 1.0,
            miter_limit: 0.0,
            dashes: vec![],
            dash_phase: 0.0,
        };

        let synapse_weight_stroke = StrokeParams {
            cap: 0,
            join: 0,
            thickness: 3.0,
            miter_limit: 0.0,
            dashes: vec![],
            dash_phase: 0.0,
        };

        // Fill background
        let path = Path::new(ctx, FillMode::Winding);
        path.add_rectangle(ctx, 0., 0., draw_params.area_width, draw_params.area_height);
        path.end(ctx);
        draw_params.context.fill(&path, &bg_brush);

        if let Some(brain_draw) = &*self.brain_draw.borrow() {
            let neuron_radius = 20.0;
            let width = draw_params.area_width / (brain_draw.size.0 as f64 + 1.0);
            let height = draw_params.area_height / (brain_draw.size.1 as f64 + 1.0);
            let radius = width.min(height) / 3.0;

            // Draw synapses
            for synapse_draw in &brain_draw.synapses {
                let (x1, y1) = screen_pos(synapse_draw.start, (width, height));
                let (x2, y2) = screen_pos(synapse_draw.end, (width, height));

                // move horizontally out of the neuron
                //let (x1, x2) = (x1 + radius, x2 - radius);

                // Draw weight arm
                let scale = crate::math::halfmoid(synapse_draw.weight.abs()) as f64 / 2.0;
                let w = (x2-x1) * scale;
                let h = (y2-y1) * scale;

                let path = Path::new(ctx, FillMode::Winding);
                path.new_figure(ctx, x1, y1);
                path.line_to(ctx, x1+w, y1+h);
                path.end(ctx);

                let brush = Brush::Solid(value_color(
                    if synapse_draw.weight > 0.0 { 1.0 } else { -1.0 }
                ));
                draw_params.context.stroke(&path, &brush, &synapse_weight_stroke);

                // Draw current synapse value over the weight
                let path = Path::new(ctx, FillMode::Winding);
                path.new_figure(ctx, x1, y1);
                path.line_to(ctx, x2, y2);
                path.end(ctx);

                let brush = Brush::Solid(value_color(synapse_draw.value));
                draw_params.context.stroke(&path, &brush, &synapse_stroke);
            }            

            // Draw Neurons
            for neuron_draw in &brain_draw.neurons {
                let (x, y) = screen_pos(neuron_draw.pos, (width, height));

                let path = Path::new(ctx, FillMode::Winding);
                path.new_figure_with_arc(
                    ctx,
                    x, y,
                    radius,
                    // start angle, sweep
                    0.0, std::f64::consts::TAU,
                    // negative
                    false,
                );
                path.end(ctx);

                let brush = Brush::Solid(value_color(neuron_draw.value));
                draw_params.context.fill(&path, &brush);
            }
        }
    }
}

fn start_ui (
    app_rx: mpsc::Receiver<AppEvent>,
    gui_tx: mpsc::Sender<GuiEvent>,
    config: Dict,
) {
    let tx = Rc::new(gui_tx);

    // Initialize the UI library
    let ui = UI::init().expect("Couldn't initialize UI library");

    // Create a window into which controls can be placed
    let mut win = libui::prelude::Window::new(&ui.clone(), "Test App", 200, 200, WindowType::NoMenubar);

    // Main window layout, stacking section rows
    let mut root_col = VerticalBox::new();

    // Main component, holding each panel stacked horizontally
    let mut main_row = HorizontalBox::new();
    //main_row.set_padded(true);



    let mut actions_group = Group::new("Actions");
    let mut actions_vbox = VerticalBox::new();

    let mut pause_button = Button::new("Pause/Resume");
    pause_button.on_clicked({
        let tx = tx.clone();
        move |_| {
            tx.send(GuiEvent::Pause);
        }
    });

    let mut load_button = Button::new("Load");
    load_button.on_clicked({
        let tx = tx.clone();
        move |_| {
            tx.send(GuiEvent::Load(None));
        }
    });

    let mut save_button = Button::new("Save");
    save_button.on_clicked({
        let tx = tx.clone();
        move |_| {
            tx.send(GuiEvent::Save(None));
        }
    });

    let mut quit_button = Button::new("Quit");
    quit_button.on_clicked({
        let ui = ui.clone();
        let tx = tx.clone();
        move |_| {
            ui.quit();
            tx.send(GuiEvent::Quit);
        }
    });

    actions_vbox.append(pause_button, LayoutStrategy::Compact);
    actions_vbox.append(load_button, LayoutStrategy::Compact);
    actions_vbox.append(save_button, LayoutStrategy::Compact);
    actions_vbox.append(quit_button, LayoutStrategy::Compact);
    actions_group.set_child(actions_vbox);
    main_row.append(actions_group, LayoutStrategy::Compact);


    struct WStatEntry {
        name: String,
        value: f32,
        index: usize,
        //control: Control,
    }


    let mut config_group = Group::new("Configuration");
    let mut config_box = VerticalBox::new();

    let mut config_entries_map = HashMap::<String, Entry>::new();

    for (name, value) in config.entries.into_iter() {
        let mut row = HorizontalBox::new();
        let mut label = Label::new(&name);
        let mut entry = Entry::new();
        entry.set_value(&value.to_string());

        let lastval = std::cell::Cell::new(value);
        config_entries_map.insert(name.clone(), entry.clone());

        entry.on_changed({
            let tx = tx.clone();
            let name = name.clone();
            let mut entry = entry.clone();
            move |input| {
                if let Ok(newval) = input.parse() {
                    tx.send(GuiEvent::ConfigChange(name.clone(), newval));
                    lastval.set(newval);
                } else {
                    entry.set_value(&lastval.get().to_string());
                }
            }
        });

        row.append(label, LayoutStrategy::Stretchy);
        row.append(entry, LayoutStrategy::Compact);
        config_box.append(row, LayoutStrategy::Compact);
    }

    config_group.set_child(config_box.clone());
    main_row.append(config_group, LayoutStrategy::Compact);

    root_col.append(main_row, LayoutStrategy::Compact);


    let mut wstat_group = Group::new("World Stats");
    let mut wstat_box = VerticalBox::new();
    let mut wstat_entries = Vec::<WStatEntry>::new();

    // TODO: Draw historic data

    wstat_group.set_child(wstat_box.clone());
    root_col.append(wstat_group, LayoutStrategy::Compact);



    let mut bstat_group = Group::new("Bichito");
    let mut bstat_box = VerticalBox::new();
    let mut bstat_entries = Vec::<WStatEntry>::new();

    // TODO: Draw historic data

    bstat_group.set_child(bstat_box.clone());
    root_col.append(bstat_group, LayoutStrategy::Compact);



    let brain_draw_ref = Rc::new(RefCell::new(None));

    let mut brain_group = Group::new("Brain");
    let brain_area = Area::new(Box::new(BrainDrawHandle {
        brain_draw: brain_draw_ref.clone()
    }));
    brain_group.set_child(brain_area.clone());
    root_col.append(brain_group, LayoutStrategy::Stretchy);


    // Actually put the controls on the window and show it
    win.set_child(root_col);
    win.show();


    // Rather than just invoking ui.run(), using EventLoop gives a lot more control
    // over the user interface event loop.
    // Here, the on_tick() callback is used to update the view against the state.
    let mut event_loop = ui.event_loop();
    event_loop.on_tick({
        move || {
            while let Ok(event) = app_rx.try_recv() {
                match event {
                    AppEvent::WorldStats(dict) => {
                        unsafe { // Clear the stats panel
                            use libui_ffi::{uiBox, uiBoxDelete};

                            let ptr = wstat_box.ptr();

                            for _ in (0..wstat_entries.len()).rev() {
                                uiBoxDelete(ptr, 0);
                            }
                        }

                        wstat_entries = dict.entries.into_iter().enumerate().map(
                            |(index, (name, value))| {
                                let mut row = HorizontalBox::new();
                                let mut label = Label::new(&format!("{}: {}", name, value));

                                row.append(label, LayoutStrategy::Stretchy);
                                wstat_box.append(row, LayoutStrategy::Compact);

                                WStatEntry { name, value, index: index }
                            }
                        ).collect();
                    },
                    AppEvent::BichoStats(dict) => {
                        unsafe { // Clear the stats panel
                            use libui_ffi::{uiBox, uiBoxDelete};

                            let ptr = bstat_box.ptr();

                            for _ in (0..bstat_entries.len()).rev() {
                                uiBoxDelete(ptr, 0);
                            }
                        }

                        bstat_entries = dict.entries.into_iter().enumerate().map(
                            |(index, (name, value))| {
                                let mut row = HorizontalBox::new();
                                let mut label = Label::new(&format!("{}: {}", name, value));

                                row.append(label, LayoutStrategy::Stretchy);
                                bstat_box.append(row, LayoutStrategy::Compact);

                                WStatEntry { name, value, index: index }
                            }
                        ).collect();
                    },
                    AppEvent::ConfigUpdate(dict) => {
                        for (name, value) in dict.entries.into_iter() {
                            if let Some(entry) = config_entries_map.get_mut(&name) {
                                entry.set_value(&value.to_string());
                            }
                        }
                    },
                    AppEvent::Brain(draw_opt) => {
                        *brain_draw_ref.borrow_mut() = draw_opt;
                        brain_area.queue_redraw_all();
                    }
                    _ => println!("Received App Event: {:?}", event)
                }
            }
        }
    });

    // Use run_delay so that the event loop ticks every #N ms
    event_loop.run_delay(100);
}


fn setup(
    mut commands: Commands,
    config: Res<Config>,
) {
    let config_dict = Dict {
        entries: config.entries().into_iter().map(|entry| (entry.name, entry.value)).collect()
    };
    let (app_tx, app_rx) = mpsc::channel();
    let (gui_tx, gui_rx) = mpsc::channel();

    thread::spawn(move || {
        start_ui(app_rx, gui_tx, config_dict);
    });

    app_tx.send(AppEvent::WorldStats(Dict{
        entries: vec![("foo".to_string(), 42.0), ("bar".to_string(), 69.0)],
    }));

    commands.insert_resource(GuiResource {
        app_tx: Mutex::new(app_tx),
        gui_rx: Mutex::new(gui_rx),
    });
}

fn update(
    mut config: ResMut<Config>,
    stats: Res<Stats>,
    focus_q: Query<(
        &Transform,
        &bicho::components::Energy,
        &bicho::components::Mass,
        &bicho::components::Sensor,
        &bicho::components::BrainComponent,
    ), With<Focus>>,
    gui: Res<GuiResource>,
    mut save_events: EventWriter<SaveEvent>,
    mut load_events: EventWriter<LoadEvent>,

    game_state: Res<State<GameState>>,
    mut next_game_state: ResMut<NextState<GameState>>,
) {
    if let Ok(rx) = gui.gui_rx.try_lock() {
        while let Ok(msg) = rx.try_recv() {
            match msg {
                GuiEvent::ConfigChange(name, value) => {
                    config.update(&name, value);
                },
                GuiEvent::Save(file_opt) => {
                    if let Some(file) = file_opt {
                        config.save_file = Some(file);
                    }
                    save_events.send(SaveEvent);
                },
                GuiEvent::Load(file_opt) => {
                    if let Some(file) = file_opt {
                        config.save_file = Some(file);
                    }
                    load_events.send(LoadEvent);
                },
                GuiEvent::Pause => {
                    let next_state = game_state.get().toggle();
                    next_game_state.set(next_state);
                },
                msg => {
                    println!("Unsupported Gui Event: {:?}", msg);
                }
            }
        }
    }

    if let Ok(tx) = gui.app_tx.try_lock() {
        if let Some(sample) = stats.history.iter_samples().next() {
            let dict = Dict {
                entries: vec![
                    ("Bicho Count".to_string(), sample.bicho_count as f32),
                    ("Average Mass".to_string(), sample.average_mass as f32),
                    ("Average Age".to_string(), sample.average_age as f32),
                ]
            };
            tx.send(AppEvent::WorldStats(dict));
        }

        if let Some((transform, energy, mass, sensor, brain)) = focus_q.iter().next() {
            let dict = Dict {
                entries: vec![
                    ("Energy".to_string(), energy.amount.0),
                    ("Mass".to_string(), mass.0.0),
                    ("Sensor Mag".to_string(), sensor.magnitude),
                    ("Sensor Aff".to_string(), sensor.affinity),
                ]
            };
            tx.send(AppEvent::BichoStats(dict));
            tx.send(AppEvent::Brain(Some(brain.0.compute_draw())));
        } else {
            tx.send(AppEvent::BichoStats(Default::default()));
            tx.send(AppEvent::Brain(None));
        }
    }
    // TODO: Check if gui_rx has events and send them to a bevy event resource
}

pub struct GuiPlugin;
impl Plugin for GuiPlugin {
    fn build(&self, app: &mut App) {
        app
            .add_systems(Startup, setup)
            .add_systems(Update, update);
    }
}

