#![allow(warnings)]

//! Shows how to iterate over combinations of query results.

use bevy::{
    prelude::*,
    winit::{WinitSettings, UpdateMode},
    state::app::AppExtStates as _,
};

use bevy_pancam::{PanCam, PanCamPlugin};


mod constants;
mod units;
mod math;

mod neat;

mod config;
mod world;
mod brain;
mod gene;
mod bicho;
mod spawner;
mod stats;
mod deadpool;
mod chemistry;
mod gui;
mod store;

mod map;

pub use prelude::*;
pub mod prelude {
    use crate::*;
    pub use config::Config;
}

#[derive(Debug, Clone, Copy, Default, Eq, PartialEq, Hash, States)]
pub enum GameState {
    #[default]
    Paused,
    Running
}

impl GameState {
    pub fn toggle (self) -> Self {
        match self {
            Self::Paused => Self::Running,
            Self::Running => Self::Paused,
        }
    }
}

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugins(bevy_egui::EguiPlugin)
        .add_plugins(PanCamPlugin::default())
        .insert_state(GameState::Running)

        .add_plugins(config::Plugin)
        .add_plugins(world::WorldPlugin)
        .add_plugins(stats::StatsPlugin)
        .add_plugins(bicho::BichoPlugin)
        .add_plugins(spawner::Plugin)
        .add_plugins(deadpool::Plugin)
        .add_plugins(gui::GuiPlugin)
        .add_plugins(map::Plugin)
        .add_plugins(store::Plugin)

        .insert_resource(ClearColor(Color::BLACK))
        .insert_resource(WinitSettings {
            focused_mode: UpdateMode::Continuous,
            unfocused_mode: UpdateMode::Continuous,
            ..default()
        })
        .add_systems(Startup, setup)
        .run();
}

fn setup(
    mut commands: Commands,
    mut images: ResMut<Assets<Image>>,
) {
    let scale = 1.0;
    commands.spawn(Camera2dBundle::default())
        .insert(PanCam::default());
}
