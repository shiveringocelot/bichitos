use bevy::{
    prelude::*,
};

#[derive(Debug, Clone, Resource, serde::Serialize, serde::Deserialize)]
pub struct Config {
    /// Where to save the current game
    pub save_file: Option<String>,

    /// How many cells grow in an empty map
    pub cell_grow_rate: f32,

    /// How much proportion of cells die each time unit
    #[serde(default = "defaults::cell_decay_rate")]
    pub cell_decay_rate: f32,

    /// How much proportion of cells die each time unit
    #[serde(default = "defaults::cell_spread_rate")]
    pub cell_spread_rate: f32,

    /// How much proportion of cells die each time unit
    #[serde(default = "defaults::friction")]
    pub friction: f32,

    /// How much proportion of cells die each time unit
    #[serde(default = "defaults::neuron_energy_use")]
    pub neuron_energy_use: f32,

    /// How many cells to try to spawn a bicho on every tick
    pub spawn_count: i32,
}

pub struct Entry {
    pub name: String,
    pub value: f32,
}

impl Config {
    pub fn entries (&self) -> impl IntoIterator<Item=Entry> {
        vec![
            Entry{ name: "cell_grow_rate".to_string(), value: self.cell_grow_rate },
            Entry{ name: "cell_decay_rate".to_string(), value: self.cell_decay_rate },
            Entry{ name: "cell_spread_rate".to_string(), value: self.cell_spread_rate },
            Entry{ name: "friction".to_string(), value: self.friction },
            Entry{ name: "neuron_energy_use".to_string(), value: self.neuron_energy_use },
        ]
    }

    pub fn update (&mut self, name: &str, value: f32) {
        match name {
            "cell_grow_rate" => {
                self.cell_grow_rate = value;
            },
            "cell_decay_rate" => {
                self.cell_decay_rate = value;
            },
            "cell_spread_rate" => {
                self.cell_spread_rate = value;
            },
            "friction" => {
                self.friction = value;
            },
            "neuron_energy_use" => {
                self.neuron_energy_use = value;
            },
            _ => {
                eprintln!("Unknown config field name: {:?}", name);
            }
        }
    }
}

impl Default for Config {
    fn default () -> Self {
        Config {
            save_file: None,
            cell_grow_rate: 500.0,
            cell_decay_rate: defaults::cell_decay_rate(),
            cell_spread_rate: defaults::cell_spread_rate(),
            friction: defaults::friction(),
            neuron_energy_use: defaults::neuron_energy_use(),
            spawn_count: 50,
        }
    }
}

mod defaults {
    pub fn cell_decay_rate () -> f32 { crate::constants::MAP_LOSS }
    pub fn cell_spread_rate () -> f32 { crate::constants::MAP_DIFUSSION }
    pub fn friction () -> f32 { 1.0 - crate::constants::DAMPING }

    pub fn neuron_energy_use () -> f32 { crate::constants::LIVING_COST / 32.0 }
}

pub struct Plugin;
impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut App) {
        app.insert_resource(Config::default());
    }
}