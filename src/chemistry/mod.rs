
use rand::{thread_rng, Rng};

use crate::math;

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Atom {
    /// Magnitude, ranges from 0 to ∞
    pub mag: f32,
    /// ranges from 0.0 to 1.0
    pub phase: f32,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub struct Reaction {
    pub mag: f32,
    pub phase: f32,
}

fn lerp (a: f32, b: f32, t: f32) -> f32 {
    return (1.0 - t) * a + t * b;
}

fn wrap (a: f32) -> f32 {
    if a < 0.0 { 1.0 - (-a % 1.0) }
    else { a % 1.0 }
}

const BIAS: f32 = 0.0625;

impl Atom {
    pub fn new (mag: f32, phase: f32) -> Atom {
        Atom { mag: mag, phase: phase }
    }
    pub fn zero () -> Atom {
        Atom { mag: 0.0, phase: 0.0 }
    }

    pub fn with_mag (self, mag: f32) -> Atom {
        Atom { mag, phase: self.phase }
    }

    pub fn with_magnitude (self, mag: f32) -> Atom {
        self.with_mag(mag)
    }

    pub fn scale (self, scale: f32) -> Atom {
        Atom { mag: self.mag * scale, phase: self.phase }
    }

    pub fn normalize (self) -> Atom {
        self.with_mag(1.0)
    }

    fn mag_bias (self) -> f32 {
        return self.mag + 0.0625;
    }



    pub fn random () -> Atom {
        let mut rng = thread_rng();
        Atom { mag: 1.0, phase: rng.gen_range(0.0 .. 1.0) }
    }


    pub fn affinity (self, b: Atom) -> f32 {
        let a = self;
        let mut dist = 2.0 * wrap(b.phase - self.phase);
        if dist > 1.0 { dist -= 2.0; }
        return 1.0 - dist.abs();
    }

    pub fn react_simple (self, b: Atom) -> Reaction {
        let mag = b.mag / (self.mag + BIAS);

        let dist = wrap(b.phase - self.phase) * 2.0;
        let dist = if dist > 1.0 { dist - 2.0 } else { dist };

        return Reaction {
            mag: mag,
            //phase: wrap(self.phase + dist * mag * 1.0)
            phase: self.phase - b.phase,
        };
    }

    pub fn shift_phase_mut (&mut self, value: f32) {
        self.phase = wrap(self.phase + value);
    }

    /// Lossily encode the atom as string
    pub fn encode_string(&self) -> String {
        let format_number = |num: f32| {
            let formatted = format!("{:.5}", num);
            formatted.trim_end_matches('0').trim_end_matches('.').to_string()
        };
        format!("{}:{}", format_number(self.mag), format_number(self.phase))
    }

    pub fn decode_string(s: &str) -> Option<Self> {
        let parts: Vec<&str> = s.split(':').collect();
        let a = parts.get(0)?.parse::<f32>().ok()?;
        let b = parts.get(1)?.parse::<f32>().ok()?;
        Some(Atom { mag: a, phase: b })
    }
}

impl Atom {
    /// Get fully saturated atom color computed from its phase
    pub fn get_chroma_color (self) -> [f32; 3] {
        /*
        let [u, v] = self.get_chroma();

        let r = (-u).max(0.0);
        let g = u.max(0.0);
        let b = (v + 1.0) / 2.0;

        [r, g, b]
        */

        let f6 = self.phase * 6.0;

        // Ascending and descending slopes
        let asc = f6.fract();
        let des = 1.0 - asc;

        match f6 as i32 {
            0 => [1.0, asc, 0.0],
            1 => [des, 1.0, 0.0],
            2 => [0.0, 1.0, asc],
            3 => [0.0, des, 1.0],
            4 => [asc, 0.0, 1.0],
            5 => [1.0, 0.0, des],
            _ => [0.0, 0.0, 0.0],
        }
    }

    /// Get the real atom color from its phase and magnitude
    pub fn get_color (self) -> [f32; 3] {
        const STEPS: f32 = 2.0;
        const BASE: f32 = 1.0 - (1.0 / STEPS);

        let [r, g, b] = self.get_chroma_color();
        let a = 1.0 - 1.0/(self.mag+1.0);
        return [r*a, g*a, b*a];

        // Luminance. Calculated by scaling the magnitude to 0..1
        // 1 = white, 0 = black, 0.5 = full saturation
        let l = 1.0 - BASE.powf(self.mag);

        // v: closest pure luminance value (white or black)
        // t: interpolation between the color and v
        let (t, v) = if l < 0.5 {
            (1.0 - l*2.0, 0.0)
        } else {
            (l * 2.0 - 1.0, 1.0)
        };

        [lerp(r, v, t), lerp(g, v, t), lerp(b, v, t)]
    }
}

impl Reaction {
    pub fn combine (self, b: Reaction) -> Reaction {
        Reaction{
            mag: self.mag + b.mag,
            phase: self.phase + b.phase * b.mag
        }
    }
}

impl std::default::Default for Reaction {
    fn default () -> Self {
        Reaction {mag: 0.0, phase: 0.0}
    }
}

impl std::ops::Mul<f32> for Atom {
    type Output = Self;

    fn mul(self, rhs: f32) -> Self::Output {
        self.scale(rhs)
    }
}

pub fn react_simple (atoms: impl IntoIterator<Item=impl std::borrow::Borrow<Atom>>) -> Atom {
    use std::f32::consts::TAU;

    let mut pos = (0.0, 0.0);
    let mut count = 0;

    for atom in atoms.into_iter() {
        let atom = atom.borrow();
        let atom_pos = math::polar_to_cartesian(atom.phase * TAU, atom.mag);
        pos.0 += atom_pos.0;
        pos.1 += atom_pos.1;
        count += 1;
    }

    if count == 0 {
        return Atom::zero();
    }

    let polar = math::cartesian_to_polar(pos.0, pos.1);
    return Atom {
        mag: polar.1 as f32,
        phase: wrap(polar.0 / TAU),
    };
}

pub fn react (atoms: impl IntoIterator<Item=impl std::borrow::Borrow<Atom>>) -> Atom {
    return react_simple(atoms);
}

/*
impl std::ops::Add<Self> for Atom {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        self.apply(self.react(rhs))
    }
}
*/


#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! atom {
        ($mag:expr, $phase:expr) => (Atom {mag: $mag, phase: $phase})
    }

    macro_rules! assert_close {
        ($a:expr, $b:expr) => {{
            let (a, b) = ($a, $b);
            let diff = (a-b).abs();
            if (diff > 1e-6) {
                panic!("assertion `left ~= right` failed\n  left: {:?}\n  right: {:?}", a, b);
            }
        }}
    }

    #[test]
    fn atom_reactions () {
        assert_eq!(react_simple([atom!(1.0, 0.3), atom!(1.0, 0.3)]), atom!(2.0, 0.3));
        assert_close!(react_simple([atom!(1.0, 0.0), atom!(1.0, 0.5)]).mag, 0.0);
        assert_close!(react_simple([atom!(1.0, 0.0), atom!(1.0, 0.25)]).phase, 0.125);
    }

    #[test]
    fn atom_encoding () {
        // Checks that a value doesn't change after encoding and decoding
        macro_rules! assert_encoding {
            ($a:expr) => {{
                let a = $a;
                let b = Atom::decode_string(&a.encode_string());
                assert_eq!(b, Some(a));
            }}
        }

        assert_eq!(atom!(1.0, 0.5).encode_string(), "1:0.5");
        assert_encoding!(atom!(1.0, 0.5));
        assert_encoding!(atom!(12345.0, 0.0001));

        // Last 2 digits get rounded
        assert_eq!(atom!(1.0, 0.1234567).encode_string(), "1:0.12346");
    }
}