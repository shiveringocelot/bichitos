
use bevy::{
    prelude::*,
    math::vec2,
};

use rand::{thread_rng, Rng};
use crate::{
    constants, units, math,

    config::Config,
    world::{Map, Time},
    math::TAU,
    chemistry::{Atom, react},
    deadpool::{Pool, DeathEvent},
    bicho::{SpawnEvent},
    gene::Gene,
};

pub mod component {
    use super::*;


    #[derive(Copy, Clone)]
    pub enum Shape { Edge, Center }

    #[derive(Component)]
    pub struct Spawner {
        pub speed: f32,

        /// Random change of the angle per generation
        pub jitter: f32,

        pub atom: Atom,
        pub range: f32,
        pub shape: Shape,
    }

    impl Spawner {
        pub fn random_position (&self, pos: Vec2, rng: &mut impl Rng) -> Vec2 {
            let value = rng.gen_range(0.0 .. 1f32);

            let distance = match self.shape {
                // Use the circle equation to skew to the edge
                //Shape::Edge => (1.0 - value.powi(2)).sqrt(),

                Shape::Edge => 1.0,

                // Skew to the center
                Shape::Center => value.powi(2),
            } * self.range;

            let rot = Vec2::from_angle(rng.gen_range(0.0 .. TAU));

            pos + rot.rotate(vec2(0.0, distance))
        }

        pub fn random_even_position (&self, pos: Vec2, rng: &mut impl Rng) -> Vec2 {
            let value = rng.gen_range(0.0 .. 1f32);

            // I don't know if this should be sqrt, arctan or whatever.
            // I'm trying to sample the area of the circle evenly
            let distance = value.sqrt() * self.range;

            let rot = Vec2::from_angle(rng.gen_range(0.0 .. TAU));

            pos + rot.rotate(vec2(0.0, distance))
        }
    }

    #[derive(Component)]
    pub struct Position(pub Vec2);

    #[derive(Component)]
    /// Global angle of movement
    pub struct Heading(pub f32);
}

pub use component::*;

const SPAWNER_COUNT: usize = 4;

fn setup (
    mut commands: Commands,
    map: Res<Map>,
) {
    let mut rng = thread_rng();
    let size = map.size as f32 / 2.0;

    let params = [
        (Shape::Center, 0.0),
        (Shape::Center, 0.25),
        (Shape::Center, 0.5),
        (Shape::Center, 0.75),
    ];

    for (shape, phase) in params.into_iter() {
        commands.spawn((
            Spawner {
                speed: 2.0,
                jitter: 0.5,
                atom: Atom::new(1.0, phase),
                range: 20.0,
                shape: shape,
            },
            Pool::new(),
            Heading(rng.gen_range(0.0 .. TAU)),
            Position(vec2(
                rng.gen_range(-size..size),
                rng.gen_range(-size..size),
            )),
        ));
    }
}

fn update (
    time: Res<Time>, 
    stats: Res<crate::stats::Stats>, 
    config: Res<Config>, 
    mut map: ResMut<Map>,
    mut query: Query<(&Spawner, &mut Position, &mut Heading, &Pool)>,
    mut spawn_events: EventWriter<SpawnEvent>,
) {
    if !time.is_generation_start() { return; }

    let mut rng = thread_rng();

    let cell_spawn_count = {
        let cell_mass = map.compute_mass();

        let cell_count = (map.size * map.size) as f32;

        let total_capacity = constants::CELL_CAPACITY * cell_count;
        let free_capacity = total_capacity - cell_mass;

        let grow_rate = config.cell_grow_rate / total_capacity;

        (free_capacity * grow_rate) as usize / SPAWNER_COUNT
    };

    let bicho_spawn_count = config.spawn_count as usize / SPAWNER_COUNT;

    for (spawner, mut pos, mut heading, pool) in query.iter_mut() {
        heading.0 += rng.gen_range(0.0 .. spawner.jitter) - spawner.jitter / 2.0;
        pos.0 += Vec2::from_angle(heading.0).rotate(Vec2::Y * spawner.speed);

        for _ in 0..cell_spawn_count {
            let pos = spawner.random_position(pos.0, &mut rng);

            let atom = map.read(pos);
            map.write(pos, react([atom, spawner.atom]));
        }

        for _ in 0..bicho_spawn_count {
            let position = spawner.random_even_position(pos.0, &mut rng);

            let value = map.read(position).mag;
            if value > constants::BIRTH_COST {
                let do_sample = !pool.is_empty() &&
                    rng.gen_bool(constants::DEADPOOL_SAMPLE_RATE as f64);

                let gene = if do_sample {
                    pool.sample().clone_mutated()
                } else {
                    let mut g = Gene::new();
                    g.mutate();
                    g
                };

                spawn_events.send(SpawnEvent {
                    gene, position,
                    energy: units::Energy(1.0),
                });
            }
        }
    }
}

fn death_event_listener (
    mut commands: Commands,
    mut events: EventReader<DeathEvent>,
    mut query: Query<(Entity, &Position, &mut Pool)>,
) {
    let mut rng = thread_rng();

    for event in events.read() {
        // Find closest pool
        let mut nearest_entity = None;

        for (entity, position, _) in &query {
            let dist = position.0.distance(event.position);

            let replace = match nearest_entity {
                Some((_, prev_dist)) => dist < prev_dist,
                None => true,
            };

            if replace {
                nearest_entity = Some((entity, dist));
            }
        }

        // Add sample to the pool
        let (entity, _) = nearest_entity.unwrap();
        let (_, _, mut pool) = query.get_mut(entity).unwrap();

        let fitness = math::sigmoid(event.age) + math::sigmoid(event.child_count as f32) * 0.5;
        pool.add_sample(&event.gene, fitness, &mut rng);
    }
}

pub struct Plugin;
impl bevy::prelude::Plugin for Plugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Startup, setup)
            .add_systems(Update, (death_event_listener, update));
    }
}