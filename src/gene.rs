
use std::collections::{HashMap, HashSet};
use rand::{thread_rng, Rng, distributions::{Distribution, WeightedIndex}};

use crate::{
    constants,
    brain::Brain,
    chemistry::Atom,
};

#[derive(Clone)]
pub struct Gene {
    pub brain: Brain,
    pub mutation_rate: MutationRates,
    pub skin: Atom,
    pub memory_cell_count: usize,
}

#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct MutationRates {
    /// How likely is a mutation in a bicho division
    rate: f32,

    /// How much the weight changes can change when mutated
    scale: f32,

    /// How much weight topology
    topo_weight: f32,
}

#[derive(Debug)]
enum Mutation {
    ConnectNeurons(usize, usize),

    ActivateSynapse(usize),
    UpdateSynapse(usize),
    SplitSynapse(usize),
    RemoveSynapse(usize),

    AddMemory,
    ChangeMutationRate,
}

struct MutationCandidate {
    mutation: Mutation,
    cost: f32,
}

impl Gene {
    pub fn new () -> Self {
        let mut brain = Brain::new(1);
        brain.randomize_weights();
        Gene {
            brain,
            mutation_rate: MutationRates {
                rate: constants::INITIAL_MUTATION_RATE,
                scale: 1.0,
                topo_weight: 1.0,
            },
            skin: Atom::random(),
            memory_cell_count: 1,
        }
    }

    fn compute_mutations (&self) -> Vec<MutationCandidate> {
        let genome = &self.brain.genome;
        let layer_count = genome.layers.len();

        struct NeuronConnection {
            index: usize,
            // If this is none, the connection is disabled
            weight: Option<f32>,
        }

        // Collect all neuron pairs in hashmap.
        // For every neuron pair (usize, usize), it stores whether they are connected (bool)
        let mut neuron_pairs = HashMap::new();

        // Four nested loops!! Daaamn
        for start_layer in (0 .. layer_count) {
            // Neurons can connect to any neuron in any forward layer
            for end_layer in ((start_layer+1) .. layer_count) {

                // Insert neuron pairs
                for start_neuron in &genome.layers[start_layer].nodes {
                    for end_neuron in &genome.layers[end_layer].nodes {
                        neuron_pairs.insert((*start_neuron, *end_neuron), None);
                    }
                }
            }
        }

        for (index, connection) in genome.connections.iter().enumerate() {
            neuron_pairs.insert(
                (connection.input, connection.output),
                Some(NeuronConnection{
                    index,
                    weight: connection.enabled.then(|| connection.weight),
                })
            );
        }

        let mut candidates = vec![];
        for ((input, output), connection) in neuron_pairs {
            if let Some((connection)) = connection {
                if let Some(weight) = connection.weight {
                    candidates.push(MutationCandidate {
                        mutation: Mutation::UpdateSynapse(connection.index),
                        cost: 0.5,
                    });
                    candidates.push(MutationCandidate {
                        mutation: Mutation::RemoveSynapse(connection.index),
                        // the cost of removing a synapse depends on its weight
                        // halfmoid scales the weight even more, and bounds it to (0,1)
                        // shift the value to never have a cost of 0
                        cost: crate::math::halfmoid(weight).abs() + 0.1,
                    });
                    candidates.push(MutationCandidate {
                        mutation: Mutation::SplitSynapse(connection.index),
                        cost: 2.0,
                    });
                } else {
                    candidates.push(MutationCandidate {
                        mutation: Mutation::ActivateSynapse(connection.index),
                        cost: 1.0,
                    });
                }
            } else {
                candidates.push(MutationCandidate {
                    mutation: Mutation::ConnectNeurons(input, output),
                    cost: 1.0,
                })
            }
        }

        candidates.push(MutationCandidate {
            mutation: Mutation::ChangeMutationRate,
            // Relative cost stay roughly the same regardless of neuron count
            cost: 1.0 / genome.nodes.len() as f32,
        });

        candidates
    }

    pub fn mutate (&mut self) {
        // doing (rate*2) attempts allows each individual mutation
        // to have a 50% chance of ocurring
        let rate = self.mutation_rate.rate;
        let count = (rate * 2.0).ceil() as u32;
        let single_rate = rate / count as f32;

        let mut candidates = self.compute_mutations();
        let mut used_candidates = HashSet::new();

        let genome = &mut self.brain.genome;

        let mut rng = thread_rng();
        let dist = WeightedIndex::new(candidates.iter().map(|c| 1.0 / c.cost)).unwrap();

        for _ in 0..count {
            if rng.gen_bool(single_rate as f64) {
                // Do a single mutation

                let candidate_index = dist.sample(&mut rng);

                // Don't apply the same mutation twice
                if used_candidates.contains(&candidate_index) {
                    continue;
                }
                used_candidates.insert(candidate_index);

                let candidate = &candidates[candidate_index];

                match candidate.mutation {
                    Mutation::ConnectNeurons(input, output) => {
                        let weight = rng.gen_range(-1.0 .. 1.0);
                        genome.add_connection(input, output, weight);
                    }
                    Mutation::ActivateSynapse(index) => {
                        let connection = &mut genome.connections[index];
                        connection.enabled = true;
                        connection.weight = rng.gen_range(-1.0 .. 1.0);
                    }
                    Mutation::UpdateSynapse(index) => {
                        let connection = &mut genome.connections[index];
                        let weight = connection.weight;

                        connection.weight = if weight.abs() < std::f32::EPSILON {
                            rng.gen_range(-1.0 .. 1.0)
                        } else {
                            // Ensure the range is always well formed (ordered)
                            let low = weight * -0.25;
                            let high = weight * 2.0;
                            rng.gen_range(low.min(high) .. low.max(high))
                        };
                    }
                    Mutation::SplitSynapse(index) => {
                        genome.split_connection(index);
                    }
                    Mutation::RemoveSynapse(index) => {
                        genome.connections[index].enabled = false;
                    }
                    Mutation::ChangeMutationRate => {
                        // Produces values between 0.5 and 2, centered at 1
                        let scale = 2f32.powf(rng.gen_range(-1.0..1.0) * 0.5);
                        self.mutation_rate.rate *= scale;
                    }
                    Mutation::AddMemory => {
                        todo!()
                    }
                };
            }
        }

        genome.apply_mutations();
        self.brain.update_network();
    }

    pub fn clone_mutated (&self) -> Self {
        let mut clone = self.clone();
        clone.mutate();
        clone
    }
}

/// Plain data struct that encodes a gene
#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct GeneData {
    genome: crate::neat::Genome,
    mutation_rate: MutationRates,
    skin: String,
    memory_cell_count: u32,
}

impl From<&Gene> for GeneData {
    fn from(gene: &Gene) -> Self {
        GeneData {
            genome: gene.brain.genome.clone(),
            mutation_rate: gene.mutation_rate.clone(),
            skin: gene.skin.encode_string(),
            memory_cell_count: gene. memory_cell_count as u32,
        }
    }
}

impl From<GeneData> for Gene {
    fn from(gene_data: GeneData) -> Self {
        let mut brain = Brain::new(1);
        brain.genome = gene_data.genome;
        Gene {
            brain,
            mutation_rate: gene_data.mutation_rate,
            skin: Atom::decode_string(&gene_data.skin).unwrap(),
            memory_cell_count: gene_data.memory_cell_count as usize,
        }
    }
}