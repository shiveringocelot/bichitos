
pub const PI: f32 = std::f32::consts::PI;
pub const TAU: f32 = std::f32::consts::TAU;

/// Sigmoid/Logistic function. Transforms (-inf, inf) to (0, 1)
pub fn sigmoid (x: f32) -> f32 { 1.0 / ((-x).exp() + 1.0) }

/// Sigmoid centered around 0. Transforms (-inf, inf) to (-1, 1)
pub fn halfmoid (x: f32) -> f32 { 2.0 / ((-x).exp() + 1.0) - 1.0 }

/// Similar to halfmoid but much cheaper and with a smaller derivative after 0.5
pub fn fastmoid (x: f32) -> f32 { x / (x.abs() + 1.0) }

pub fn polar_to_cartesian (radians: f32, amplitude: f32) -> (f32, f32) {
    let x = amplitude * radians.cos();
    let y = amplitude * radians.sin();
    (x, y)
}

pub fn cartesian_to_polar(x: f32, y: f32) -> (f32, f32) {
    let amplitude = (x.powi(2) + y.powi(2)).sqrt();
    let radians = f32::atan2(y, x);
    (radians, amplitude)
}